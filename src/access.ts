// 权限配置文件

export default function (initState: any) {
  console.log(initState, 'initState------------');
  // initState 里面有 role 这个属性,会根据这个role属性来去判断这个角色是不是 admin
  return {
    isAdmin: initState?.role === 'admin',
  };
}
