import { request } from 'umi';

export const GetHtml = (params: any) =>
  request(`/api/api/article/recommend`, {
    method: 'GET',
    params,
  });
