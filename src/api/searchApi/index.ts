import { request } from 'umi';

interface GETParamsType {
  page: number;
  pageSize: number;
  keyword: string;
  count: string;
  type: string;
}

// 获取搜索记录数据
export const GetSearchData = async (params: GETParamsType) => {
  return await request(`/api/api/search`, {
    method: 'GET',
    params,
  });
};

// 删除搜索记录
export const DeleteSearchItem = async (params: { id: string }) => {
  return await request(`/api/api/search/${params.id}`, {
    method: 'DELETE',
  });
};
