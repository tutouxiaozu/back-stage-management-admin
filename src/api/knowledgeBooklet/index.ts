import { request } from 'umi';

//返回数据需要使用return
export const getKnow = async (params: any) => {
  return await request(`/api/api/Knowledge`, {
    method: 'GET',
    params,
  });
};

//删除单前数据
export const delteKnow = async (id: any) => {
  return await request(`/api/api/Knowledge/${id}`, {
    method: 'DELETE',
  });
};

//添加数据
export const addKnow = async (book: any) => {
  return await request(`api/api/knowledge/book`, {
    method: 'post',
    data: book,
  });
};

// 更改数据
export const deitKnow = async (data: any) => {
  return await request(`/api/api/Knowledge/${data.id}`, {
    method: 'PATCH',
    data: data,
  });
};
