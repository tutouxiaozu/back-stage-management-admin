// 访问页面的 API 接口
import { request } from 'umi';

// 请求访问的数据
export const GetAccessData = async (params: any) => {
  return await request('/api/api/view', {
    method: 'GET',
    params,
  });
};

// 删除 访问接口的数据
export const DeleteAccess = (params: { id: string }) =>
  request(`/api/api/view/${params.id}`, {
    method: 'DELETE',
  });
