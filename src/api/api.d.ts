export interface BasePageParams {
  pageSize: number;
  limit: number;
}
