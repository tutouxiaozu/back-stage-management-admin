import { tableParams, userData, updatePswData } from '@/types';
import { searchData } from '@/types';
import { request } from 'umi';

export interface paramsType {
  page: number;
  pageSize: number;
}
//*修改用户信息
export const updateUser = (data: userData) =>
  request('/api/api/user/update', {
    method: 'POST',
    data: data,
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });

//*更改密码
export const updatePsw = (data: updatePswData) =>
  request('/api/api/user/password', {
    method: 'post',
    data: data,
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });

export const getArticle = (params: tableParams) =>
  request('/api/api/article', {
    method: 'get',
    params,
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });

export const getTags = () =>
  request('/api/api/tag', {
    method: 'get',
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });

export const getCategory = () =>
  request('/api/api/category', {
    method: 'get',
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
export const getComment = (params: searchData) =>
  request('/api/api/comment', {
    method: 'get',
    params,
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });

export const getFile = (params: tableParams) =>
  request('/api/api/file', {
    method: 'get',
    params,
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
