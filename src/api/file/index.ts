import { request } from 'umi';
import { fileParams } from './index.d';

// 获取数据 params分页(页数，在第几页)
export const _file_ = (params: any) =>
  request('/api/api/file', {
    method: 'GET',
    params,
  });

export const deleteFile = (params: any) =>
  request('/api/api/file/' + params.id, {
    method: 'DELETE',
    data: params,
  });

export const PushFile = (body: any) =>
  request('/api/api/file/upload?unique=0', {
    method: 'POST',
    data: body,
  });
