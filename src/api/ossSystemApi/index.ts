import { request } from 'umi';

export const systemOSS = async (params: any) => {
  return await request('/api/api/setting', {
    method: 'POST',
    data: params,
    skipErrorHandler: true,
  });
};
