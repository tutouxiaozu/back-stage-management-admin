// 设置页面的 api 设置

import { request } from 'umi';

// 获取系统设置 数据
export const GetSystemSettingsData = async () =>
  await request('/api/api/setting/get', {
    method: 'POST',
  });

export const SystemSettingsSava = async (params: any) => {
  return await request('/api/api/setting', {
    method: 'POST',
    data: params,
    skipErrorHandler: true,
  });
};
