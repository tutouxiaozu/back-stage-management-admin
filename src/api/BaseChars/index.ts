import { request } from 'umi';

export const getChartsData = () =>
  new Promise((resolve) => {
    setTimeout(() => {
      resolve({
        success: true,
        msg: '获取数据成功',
        data: [
          new Array(7).fill('').map(() => Math.floor(Math.random() * 100) + 1),
          new Array(7).fill('').map(() => Math.floor(Math.random() * 100) + 1),
        ],
      });
    });
  });
