import { request } from 'umi';

// 请求海报列表信息
export const GetPosterData = (params?: any) =>
  request('/api/api/poster', {
    method: 'GET',
    params,
  });
