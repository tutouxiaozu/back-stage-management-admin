import { request } from 'umi';

export const GetEmailData = (params?: any) =>
  request('/api/api/smtp', {
    method: 'GET',
    params,
  });
