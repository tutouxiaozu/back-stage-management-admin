import { request } from 'umi';
import { BasePageParams } from '../api.d';
// 获取最新文章信息
export const _getArticalList = (
  params: BasePageParams = { pageSize: 1, limit: 6 },
) => request(`/api/api/article`);
// 获取最新评论信息
export const _getTableList = (params: any = { pageSize: 1, limit: 6 }) =>
  request(`/api/api/comment`);
// 通过/拒绝
export const chengePass = (id: string, data: boolean) =>
  request(`/api/api/comment/${id}`, {
    method: 'PATCH',
    data: { pass: data },
  });
// s删除
export const _delList = (id: string) =>
  request(`/api/api/comment/${id}`, {
    method: 'DELETE',
  });

//  回复
export const replyComments = (data: any) =>
  request('/api/api/comment', {
    method: 'POST',
    data: data,
    skipErrorHandler: true,
  });

// export const _delList = (id: string) => r/equest(`/api/comment/{id}`)
