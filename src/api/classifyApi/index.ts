import { request } from 'umi';

//增加
export const add_Classify = (params: any) =>
  request(`/api/api/category`, {
    method: 'POST',
    data: params,
  });
//编辑
export const edit_Classify = (params: any) =>
  request(`/api/api/category/${params.id}`, {
    method: 'PATCH',
    data: params,
  });
//删除
export const del_Classify = (id: string) =>
  request(`/api/api/category/${id}`, {
    method: 'DELETE',
  });
