import { request } from 'umi';

//返回数据需要使用return
export const commentData = async (params: any) => {
  return await request(`/api/api/comment`, {
    method: 'GET',
    params,
  });
};

//修改后端的状态
export const editComment = async (params: any) => {
  return await request(`/api/api/comment/${params.id}`, {
    method: 'patch',
    data: params,
    skipErrorHandler: true,
  });
};

//评论回复的状态
export const huifuComment = async (params: any) => {
  return await request(`/api/api/comment`, {
    method: 'post',
    data: params,
    skipErrorHandler: true,
  });
};

//删除当前数据
export const deletComment = async (id: any) => {
  return await request(`/api/api/comment/${id}`, {
    method: 'DELETE',
  });
};
