import { request } from 'umi';

export interface RegisterData {
  name: string;
  password: string;
}

export const _register = (data: RegisterData) =>
  request(`/api/api/user/register`, {
    method: 'POST',
    data,
    isAuthorization: false, //不需要携带身份标识
  });
