import { request } from 'umi';

//添加标签
export const add_Tag = (params: any) =>
  request(`/api/api/tag`, {
    method: 'POST',
    data: params,
  });
//修改标签
export const edit_Tag = (params: any) =>
  request(`/api/api/tag/${params.id}`, {
    method: 'PATCH',
    data: params,
  });
//删除接口
export const del_Tag = (id: string) =>
  request(`/api/api/tag/${id}`, {
    method: 'DELETE',
  });
