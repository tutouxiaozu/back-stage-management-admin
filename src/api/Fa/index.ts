import { request } from 'umi';
interface Params {
  [propsName: string]: any;
}

export const articleManager = (params: Params) => {
  return request(`/api/api/article/${params.id ? params.id : ''}`, {
    method: params.method,
    data: params.data,
    params: params.params,
  });
};
export const tagsManager = (params: Params) => {
  return request(`/api/api/tag/${params.id ? params.id : ''}`, {
    method: params.method,
    data: params.data,
    params: params.params,
  });
};

export const getAllArticle = (params: Params) => {
  return request('/api/api/article', {
    method: 'get',
    params,
  });
};

export const categoryNum = (params: Params) => {
  return request('/api/api/category', {
    method: 'get',
    params,
  });
};
export const tagNum = (params: Params) => {
  return request('/api/api/tag', {
    method: 'get',
    params,
  });
};
export const categoryManager = (params: Params) => {
  return request(`/api/api/category/${params.id ? params.id : ''}`, {
    method: params.method,
    data: params.data,
    params: params.params,
  });
};
export const getPages = (params: Params) => {
  return request(`/api/api/page/${params.id ? params.id : ''}`, {
    method: params.method,
    params: params.params,
    data: params.data,
  });
};
