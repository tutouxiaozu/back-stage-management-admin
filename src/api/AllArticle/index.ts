import { request } from 'umi';

export const GetAllarticleData = async (params: any) => {
  return await request('/api/api/article', {
    method: 'GET',
    params,
  });
};
export const GetAllarticleCategoryData = async (params?: any) => {
  return await request('/api/api/category', {
    method: 'GET',
    params,
  });
};
