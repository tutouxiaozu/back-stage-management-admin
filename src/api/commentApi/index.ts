import { request } from 'umi';

//返回数据需要使用return
export const getKnow = async (params: any) => {
  return await request(`/api/api/comment`, {
    method: 'GET',
    params,
  });
};
