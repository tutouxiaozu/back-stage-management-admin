import { request } from 'umi';

export const GetHtml = (id: any) =>
  request(`/api/api/article/recommend`, {
    method: 'GET',
    id,
  });

interface Params {
  page?: number;
  pageSize?: number;
  originalname?: string;
  type?: string;
}
export const getFileList = (params?: Params) => {
  return request('/api/api/file', {
    method: 'get',
    params,
  });
};
