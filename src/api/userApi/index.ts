import { request } from 'umi';

// 请求用户列表信息
export const GetUserData = (params?: any) =>
  request('/api/api/user', {
    method: 'GET',
    params,
  });

// 修改 用户的权限
export const operationStatus = async (params: any) => {
  return await request('/api/api/user/update', {
    method: 'POST',
    data: params,
    skipErrorHandler: true,
  });
};

// 删除搜索记录
export const DeleteUserItem = async (params: { id: string }) => {
  return await request(`/api/api/user/${params.id}`, {
    method: 'DELETE',
  });
};
