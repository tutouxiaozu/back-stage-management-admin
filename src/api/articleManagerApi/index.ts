import { request } from 'umi';
import { BasePageParams } from './api.d';

//获取文章列表
export const _get_Article = (
  params: BasePageParams = { page: 1, pageSize: 6 },
) =>
  request(`/api/api/article`, {
    params,
  });
//首焦推荐
export const change_visit = (id: string, isRecommended: boolean) => {
  request(`/api/api/article/${id}`, {
    method: 'PATCH',
    data: { isRecommended },
  });
};
//草稿和发布
export const change_publish = (id: string, status: string) => {
  request(`/api/api/article/${id}`, {
    method: 'PATCH',
    data: { status },
  });
};
//查看访问接口
export const show_visit = (url: string) => {
  request(`/api/api/view/${url}`, {
    method: 'GET',
  });
};
//获取页面管理
export const _get_Page = (params: BasePageParams = { page: 1, pageSize: 6 }) =>
  request(`/api/api/page`, {
    params,
  });
//页面管理更改状态
export const change_status = (params: any) =>
  request(`/api/api/page/${params.id}`, {
    method: 'PATCH',
    params,
  });
//获取分类数据
export const _get_Classify = () => request(`/api/api/category`);
//删除数据
export const _del_Article = (id: string) => request(`/api/api/article/${id}`);
//tag
export const _get_Tag = () => request(`/api/api/tag`);
//搜索
export const searchArticle = () => request(`/api/api/search/article`);
//page页面删除
export const delPageList = (id: string) => request(`/api/api/page/${id}`);

export const change_zt = (id: string, status: boolean) => {
  request(`/api/api/page/${id}`, {
    method: 'PATCH',
    data: { status },
  });
};
