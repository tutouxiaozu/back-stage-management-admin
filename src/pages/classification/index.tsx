import { FC, useState } from 'react';
import style from './index.less';
import { useRequest } from 'ahooks';
import { _get_Classify } from '@/api/articleManagerApi';
import {
  add_Classify,
  del_Classify,
  edit_Classify,
} from '@/api/classifyApi/index';
import { Button, Form, Input, message } from 'antd';

// 分类管理页面
const Classification: FC = () => {
  const { data: classifyData } = useRequest(_get_Classify);
  const [form] = Form.useForm();
  const [str, setStr] = useState('保存');
  const [title, setTitle] = useState('添加分类');
  const [flag, setFlag] = useState(false);
  const [obj, setObj] = useState([]);
  const [id, setId] = useState('');
  const onFinish = async (values: any) => {
    // console.log(values,"111111111111111111111111");
    if (str === '保存') {
      const res = await add_Classify(values);
      if (res.statusCode === 201) {
        _get_Classify();
        form.setFieldsValue({
          label: '',
          value: '',
        });
        location.reload();
        message.success('添加成功');
      }
    } else {
      const res = await edit_Classify({
        ...obj,
        label: values.label,
        value: values.value,
      });
      if (res.statusCode === 200) {
        _get_Classify();
        form.setFieldsValue({
          label: '',
          value: '',
        });
        location.reload();
        message.success('更新成功');
      }
    }
  };

  const formItem = (item: any) => {
    // console.log(item, 'item');
    form.resetFields();
    form.setFieldsValue(item);
    setStr('更新');
    setFlag(true);
    setTitle('分类管理');
    setObj(item);
    setId(item.id);
  };
  const delTag = async () => {
    let res = await del_Classify(id);
    if (res.statusCode === 200) {
      _get_Classify();
      form.setFieldsValue({
        label: '',
        value: '',
      });
      message.success('删除成功');
      location.reload();
    }
  };
  const fanhui = () => {
    form.resetFields();
    setStr('保存');
    setTitle('添加分类');
    setFlag(false);
    // location.reload()
  };
  return (
    <div className={style.classificationWrap}>
      {/* Classification */}
      <div className={style.addClassify}>
        <h3>{title}</h3>
        {/* <div className={style.input}> */}
        <Form form={form} onFinish={onFinish} className={style.input}>
          <Form.Item label={''} name={'label'}>
            <Input placeholder="输入分类名称" />
          </Form.Item>

          <Form.Item label={''} name={'value'}>
            <Input placeholder="输入分类值(请输入英文,作为路由使用)" />
          </Form.Item>
          <Form.Item className={style.button}>
            <span>
              <Button type="primary" htmlType="submit">
                {str}
              </Button>
              <Button
                className={flag === false ? style.buttonNone : ''}
                onClick={() => fanhui()}
              >
                返回添加
              </Button>
            </span>
            <span className={style.delButton}>
              <Button
                onClick={() => delTag()}
                className={flag === false ? style.buttonNone : ''}
                danger
              >
                删除
              </Button>
            </span>
          </Form.Item>
        </Form>
        {/* </div> */}
      </div>
      <div className={style.allClassify}>
        <h3>所有分类</h3>
        <div className={style.list}>
          {classifyData &&
            classifyData.data.map((item: any, index: number) => {
              return (
                <span
                  key={index}
                  className={style.classifySpan}
                  onClick={() => formItem(item)}
                >
                  {item.label}
                </span>
              );
            })}
        </div>
      </div>
    </div>
  );
};

export default Classification;
