import { FC } from 'react';
import style from './index.less';

// 新建文章-协助编辑器
const Aditor: FC = () => {
  return <div className={style.AditorWrap}>Aditor</div>;
};

export default Aditor;
