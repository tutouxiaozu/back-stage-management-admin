export interface article {
  articleList: [];
}
export interface articleList {
  id: string;
  title: string;
  status: string;
  views: number;
  likes: string;
  createAt: string;
  label?: string;
  value?: string;
  tags: articleList[];
}
