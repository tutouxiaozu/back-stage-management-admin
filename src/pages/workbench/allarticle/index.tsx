import { FC, useEffect } from 'react';
import style from './index.less';
import type { ColumnsType } from 'antd/es/table';
import {
  _get_Article,
  _del_Article,
  _get_Classify,
  change_visit,
} from '@/api/articleManagerApi';
import {
  Table,
  Input,
  Select,
  Button,
  message,
  Popconfirm,
  Form,
  Row,
  Col,
  Tag,
  Modal,
} from 'antd';
import { useRequest } from 'ahooks';
import { ReloadOutlined } from '@ant-design/icons';
import React, { useState } from 'react';
import { request } from 'umi';
import { history } from 'umi';
import AllPage from '@/components/AllarticlePage/index';
import VisitCount from '@/components/visitCount/index';
interface DataType {
  key: React.Key;
  name: string;
  age: number;
  address: string;
  id: string;
}
// 所有文章管理页面
const Article: FC = () => {
  const { data: articleData } = useRequest(_get_Article);
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(6);
  const [total, setTotal] = useState(1);
  const [form] = Form.useForm();
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const [arr, setArr] = useState([]);
  const [classify, setClassify] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);

  //如果有值赋值给arr
  useEffect(() => {
    if (articleData) {
      setArr(articleData.data[0]);
    }
  }, [articleData]);
  const getClassify = async () => {
    const res = await _get_Classify();
    if (res.statusCode === 200) {
      setClassify(res.data);
    }
  };

  useEffect(() => {
    getClassify();
  }, []);

  useEffect(() => {
    _get_Article({ page, pageSize }).then((res) => {
      const arr = res.data[0];
      setArr(arr);
      setTotal(res.data[1]);
    });
  }, [page, pageSize]);
  //确定删除
  const confirm = (id: any) => {
    // console.log(e);
    request(`/api/api/article/${id}`, {
      method: 'DELETE',
    }).then((res) => {
      if (res.statusCode === 200) {
        message.success('删除成功');
        _get_Article({ page, pageSize }).then((res) => {
          const arr = res.data[0];
          setArr(arr);
          setTotal(res.data[1]);
          setSelectedRowKeys([]);
        });
      }
    });
  };
  const cancel = () => {
    message.error('您已取消删除');
  };
  //访问
  // selectedRows  这个是选中的多选数据列表  newSelectedRowKeys 这个是选中的多选数据列表的id
  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    // console.log('selectedRowKeys changed: ', selectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  // 当数据 没有选中的时候 就是禁用状态
  const hasSelected = selectedRowKeys.length > 0;
  //重置
  const reLoad = () => {
    location.reload();
  };
  //搜索
  const onFinish = (values: any) => {
    // console.log('Success:', values);
    // 请求列表接口
    // values是每个框里的值
    const brr = articleData.data[0].filter(
      (item: any) =>
        item.title.includes(values.title) ||
        item.status.includes(values.status),
    );

    setArr(brr);
  };
  //隐藏按钮更改发布状态
  const publish = () => {
    message.success('操作成功');
    selectedRowKeys.forEach((item: any) => {
      request(`/api/api/article/${item}`, {
        method: 'PATCH',
        data: {
          status: 'publish',
        },
      }).then((res) => {
        if (res.statusCode === 200) {
          // message.success('操作成功');
          _get_Article({ page, pageSize }).then((res) => {
            const arr = res.data[0];
            setArr(arr);
            setTotal(res.data[1]);
            setSelectedRowKeys([]);
          });
        }
      });
    });
  };
  const draft = () => {
    message.success('操作成功');
    selectedRowKeys.forEach((item: any) => {
      request(`/api/api/article/${item}`, {
        method: 'PATCH',
        data: {
          status: 'draft',
        },
      }).then((res) => {
        if (res.statusCode === 200) {
          _get_Article({ page, pageSize }).then((res) => {
            const arr = res.data[0];
            setArr(arr);
            setTotal(res.data[1]);
            setSelectedRowKeys([]);
          });
        }
      });
    });
  };
  //隐藏按钮批量首焦推荐
  const push = () => {
    message.success('操作成功');
    selectedRowKeys.forEach((item: any) => {
      request(`/api/api/article/${item}`, {
        method: 'PATCH',
        data: {
          isRecommended: true,
        },
      }).then((res) => {
        if (res.statusCode === 200) {
          _get_Article({ page, pageSize }).then((res) => {
            const arr = res.data[0];
            setArr(arr);
            setTotal(res.data[1]);
            setSelectedRowKeys([]);
          });
        }
      });
    });
  };
  const back = () => {
    message.success('操作成功');
    selectedRowKeys.forEach((item: any) => {
      request(`/api/api/article/${item}`, {
        method: 'PATCH',
        data: {
          isRecommended: false,
        },
      }).then((res) => {
        if (res.statusCode === 200) {
          _get_Article({ page, pageSize }).then((res) => {
            const arr = res.data[0];
            setArr(arr);
            setTotal(res.data[1]);
            setSelectedRowKeys([]);
          });
        }
      });
    });
  };
  //批量删除
  const allDel = () => {
    selectedRowKeys.forEach((item: any) => {
      confirm(item);
    });
  };

  const coloring = () => {
    return (
      '#' +
      Math.floor(Math.random() * 0xffffff)
        .toString(16)
        .padStart(6, '0')
    );
  };
  //弹窗
  const showModal = (id: string) => {
    setIsModalOpen(true);
    const url = `www.com/article/${id}`;
    request(`/api/api/view/url/?url=${url}`, {
      method: 'GET',
    }).then((res) => {
      console.log(res);
    });
    // show_visit(url)
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  // 分页事件
  const PagChange = (props: any) => {
    let { page, pageSize } = props;
    setPage(page);
    setPageSize(pageSize);
  };
  //编辑
  const toEditor = (record: any) => {
    history.push({
      pathname: '/article/editor',
      query: {
        id: record.id,
      },
    });
  };
  const columns: ColumnsType<DataType> = [
    {
      title: '标题',
      key: 'title',
      dataIndex: 'title',
    },
    {
      title: '状态',
      key: 'status',
      dataIndex: 'status',
      render: (status) => (
        <span>
          {status === 'publish' ? (
            <span>
              <b style={{ color: 'lightgreen' }}>●</b>已发布
            </span>
          ) : (
            <span>
              <b style={{ color: 'orange' }}>●</b>草稿
            </span>
          )}
        </span>
      ),
    },
    {
      title: '分类',
      key: 'category',
      dataIndex: 'category',
      render: (item) => {
        if (item) {
          return <Tag color={coloring()}>{item.label}</Tag>;
        }
        return null;
      },
    },
    {
      title: '标签',
      key: 'tags',
      dataIndex: 'tags',
      render: (tags) => (
        <span>
          {tags.map((item: any) => {
            return <Tag color={coloring()}>{item.label}</Tag>;
          })}
        </span>
      ),
    },
    {
      title: '阅读量',
      key: 'views',
      dataIndex: 'views',
      render: (views) => <span className={style.views}>{views}</span>,
    },
    {
      title: '喜欢数',
      key: 'likes',
      dataIndex: 'likes',
      render: (likes) => <span className={style.likes}>{likes}</span>,
    },
    {
      title: '发布时间',
      key: 'publishAt',
      dataIndex: 'publishAt',
    },
    {
      title: '操作',
      fixed: 'right',
      key: 'id',
      render: (record) => {
        return (
          <div className={style.action}>
            <Button
              onClick={() => {
                toEditor(record);
              }}
              type={'link'}
            >
              编辑
            </Button>
            <Button
              type={'link'}
              onClick={() => {
                change_visit(record.id, !record.isRecommended);
                message.success('操作成功');
                // location.reload();
                _get_Article({ page, pageSize }).then((res) => {
                  const arr = res.data[0];
                  setArr(arr);
                  setTotal(res.data[1]);
                  setSelectedRowKeys([]);
                });
              }}
            >
              {record.isRecommended ? '首焦推荐' : '撤销首焦'}
            </Button>
            <Button type={'link'} onClick={() => showModal(record.id)}>
              查看访问
            </Button>
            <Popconfirm
              title="确认删除这个文章么?"
              onConfirm={() => confirm(record.id)}
              onCancel={cancel}
              okText="确定"
              cancelText="取消"
            >
              <Button type={'link'}>删除</Button>
            </Popconfirm>
          </div>
        );
      },
    },
  ];
  return (
    <div className={style.articleManagementWrap}>
      <div
        className={style.search}
        style={{
          marginTop: '16px',
        }}
      >
        <Form form={form} onFinish={onFinish}>
          <Row>
            <Col span={8}>
              <Form.Item label={'标题'} name={'title'} className={style.form}>
                <Input placeholder="请输入标题" />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item label={'状态'} name={'status'} className={style.form1}>
                <Select
                  options={[
                    {
                      value: 'publish',
                      label: '发布',
                    },
                    {
                      value: 'draft',
                      label: '草稿',
                    },
                  ]}
                />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item
                label={'分类'}
                name={'category'}
                className={style.form1}
              >
                <Select options={classify} />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={2} offset={20}>
              <Form.Item className={style.button}>
                <Button type="primary" htmlType="submit">
                  搜索
                </Button>
                <Button
                  htmlType="button"
                  onClick={() => {
                    form.resetFields();
                  }}
                >
                  重置
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </div>
      <div className={style.table_list}>
        {/* 表格 */}
        <li className={style.list_t}>
          <span className={hasSelected ? '' : style.top_button}>
            <Button onClick={publish}>发布</Button>
            <Button onClick={() => draft()}>草稿</Button>
            <Button onClick={() => push()}>首焦推荐</Button>
            <Button onClick={() => back()}>撤销首焦</Button>
            <Popconfirm
              title="确认删除这个文章？"
              onConfirm={() => allDel()}
              okText="确认"
              cancelText="取消"
            >
              <Button danger>删除</Button>
            </Popconfirm>
          </span>
          <span className={style.top_button2}>
            <Button
              type="primary"
              onClick={() => history.push('/article/editor')}
            >
              +新建
            </Button>
            <ReloadOutlined onClick={() => reLoad()} />
          </span>
        </li>
        {articleData && (
          <Table
            // 把数组结构一下
            rowSelection={{ ...rowSelection }}
            columns={columns}
            dataSource={arr}
            pagination={false}
            rowKey={(record) => record.id}
            scroll={{
              x: 'max-content',
            }}
          />
        )}
        <div className={style.SearchRecordWrap_Pagination}>
          <AllPage children={{ total, page, pageSize, PagChange }}></AllPage>
        </div>
        <Modal
          title="访问统计"
          open={isModalOpen}
          onOk={handleOk}
          onCancel={handleCancel}
          footer={null}
        >
          <VisitCount></VisitCount>
        </Modal>
      </div>
    </div>
  );
};
export default Article;
