import { FC, CSSProperties, useMemo, useEffect, useState } from 'react';
import style from './index.less';
import BaseChart from '@/components/baseChart/index';
import BaseModal from '@/components/baseModal/index';
import { useRequest } from 'ahooks';
import { getChartsData } from '@/api/BaseChars/index';
import {
  _getArticalList,
  _getTableList,
  _delList,
  chengePass,
  replyComments,
} from '@/api/workBench/index';
import { Link, history } from 'umi';
import BaseBread from '@/components/baseBread/bread';
import {
  Card,
  Space,
  Table,
  Badge,
  Dropdown,
  Menu,
  message,
  Popconfirm,
  Popover,
  Input,
  Modal,
} from 'antd';
import { GithubOutlined, CopyrightOutlined } from '@ant-design/icons';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import useBreadRoutes from '@/hooks/useBreadcrumb/useBreadcrumb';
const breadcrumbRoutes = [{ path: '/workbench', breadcrumbName: '工作台' }];
const { Column, ColumnGroup } = Table;
const { TextArea } = Input;
const CardStyle: CSSProperties = {
  width: '33%',
  textAlign: 'center',
};
interface DataType {
  url: any;
  name: any;
  email: any;
  hostId: any;
  id: any;
  key: React.Key;
  firstName: string;
  lastName: string;
  age: number;
  address: string;
  tags: any[];
  pass: any;
}
// 工作台页面
const Workbench: FC = () => {
  // console.log(initialState);
  const [isShowModal, setIsShowModal] = useState(false);
  const [commentInfo, setCommentInfo] = useState({});
  const { loading, data: EchartsData }: any = useRequest(getChartsData, {
    // pollingInterval: 10000, //轮巡(每10秒去获取下一组数据)
  });
  const { loading: articleLoading, data: articleData } =
    useRequest(_getArticalList);
  const { data: tableData, run, refresh } = useRequest(_getTableList);
  useEffect(() => {
    run();
  }, []);
  const tableList = useMemo(() => {
    return tableData?.data[0].splice(0, 6);
  }, [tableData]);

  // console.log(tableList);
  const arr = useMemo(() => {
    return articleData?.data[0].splice(0, 6);
  }, [articleData]);
  console.log(tableList, arr);
  const userInfo = JSON.parse(window.localStorage.getItem('user_Info') as any);
  console.log(userInfo);
  // 触摸并展示内容
  const menu = (
    <Menu
      items={[
        {
          key: '1',
          label: (
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.antgroup.com"
            >
              原始内容
            </a>
          ),
        },
        {
          key: '2',
          label: (
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.aliyun.com"
            ></a>
          ),
        },
      ]}
    />
  );
  // 确定删除
  const confirm = (id: string) => {
    _delList(id).then((res: any) => {
      if (res.statusCode === 200) {
        run();
        refresh();
        message.success('删除成功！！');
      }
    });
  };
  // 取消删除
  const cancel = (e: React.MouseEvent<HTMLElement>) => {};
  // 通过
  const TongBtn = (id: string, pass: boolean) => {
    (pass = true),
      chengePass(id, pass).then((res: any) => {
        if (res.statusCode === 200) {
          run();
          refresh();
          message.success('已通过！');
        }
      });
    console.log(id);
  };
  //  拒绝
  const JuBtn = async (id: string, pass: boolean) => {
    (pass = false),
      await chengePass(id, pass).then((res: any) => {
        if (res.statusCode === 200) {
          run();
          refresh();
          message.error('拒绝！');
        }
      });
    console.log(id);
  };
  // 弹框按钮
  const handleReply = (value: any) => {
    console.log(value.text);
    const data = {
      ...commentInfo,
      content: value.text,
    };
    // console.log(data,'----------------------------------------data-------------------')
    replyComments(data);
    run();
    refresh();
    setIsShowModal(false);
    message.success('回复成功');
  };
  // 跳详情
  const toEditor = (item: any) => {
    history.push({
      pathname: '/article/editor',
      query: {
        id: item.id,
      },
    });
    message.success('跳转成功');
  };

  // 以上都是弹框
  return (
    // 工作台
    <PageHeaderWrapper
      title={
        <>
          <p style={{ fontSize: '24px' }}>
            您好,{userInfo ? userInfo.name : ''}
          </p>
          <p style={{ fontSize: '14px', fontWeight: 'none' }}>
            您的身份：
            {userInfo && userInfo.role === 'admin' ? '管理员' : '访客'}
          </p>
        </>
      }
      breadcrumb={useBreadRoutes(breadcrumbRoutes)}
    >
      <div className={style.workbenchWrap}>
        {/* 主体 */}
        <div className={style.MainBox}>
          {/* Echarts数据展示 */}
          <div className={style.BaseChartsBox}>
            {/* 封装Echarts组件 传入必要参数 */}
            <BaseChart
              type="line"
              chartData={{
                title: {
                  text: '用户统计图', //主标题
                  subtext: '周统计图', //副标题
                },
                legend: {
                  data: ['Evaporation', 'Temperature'],
                },
              }}
              series={[
                {
                  //折线图数据
                  type: 'line',
                  name: '访问量',
                  data: EchartsData?.data[0],
                },
                {
                  //柱状图数据
                  type: 'bar',
                  name: '成交量',
                  data: EchartsData?.data[1],
                },
              ]}
              xData={['周一', '周二', '周三', '周四', '周五', '周六', '周日']} //X轴标题
              loading={loading} //等待过程
              width={1200} //宽
              height={250} //高
            />
          </div>
          {/* 导航 */}
          <div className={style.navigationBox}>
            <div className={style.navigation_top_box}>
              <p>快速导航</p>
            </div>
            <div className={style.navigation_bottom_box}>
              <ul>
                <li>
                  <Link to="/article/allArticle">文章管理</Link>
                </li>
                <li>
                  <Link to="/comment">评论管理</Link>
                </li>
                <li>
                  <Link to="/fileManagement">文件管理</Link>
                </li>
                <li>
                  <Link to="/userManagement">用户管理</Link>
                </li>
                <li>
                  <Link to="/accessStatistics">访问管理</Link>
                </li>
                <li>
                  <Link to="/systemSettings">系统设置</Link>
                </li>
              </ul>
            </div>
          </div>
          {/* 卡片 */}
          <div className={style.CardBox}>
            <div className={style.Card_Top_Box}>
              <p>
                最新文章
                <span>
                  <Link to="/article/allArticle">全部文章</Link>
                </span>
              </p>
            </div>
            <div className={style.Card_Bottom_Box}>
              <Card loading={articleLoading} className={style.CardWrap}>
                {articleData?.data[0] &&
                  arr.map((item: any) => {
                    return (
                      <Card.Grid key={item.id} style={CardStyle}>
                        <div
                          className={style.ItemBox}
                          onClick={() => {
                            toEditor(item);
                          }}
                        >
                          {item.cover ? (
                            <img src={item.cover} alt="" />
                          ) : (
                            <p>文章标题</p>
                          )}
                        </div>
                        {item.title}
                      </Card.Grid>
                    );
                  })}
              </Card>
            </div>
          </div>
          {/* 表格 */}
          <div className={style.tableBox}>
            <div className={style.Table_Top_Box}>
              <p>
                最新评论
                <span>
                  <Link to="/comment">全部评论</Link>
                </span>
              </p>
            </div>
            <div className={style.Table_bottom_Box}>
              <Table
                dataSource={tableList}
                pagination={false}
                showHeader={false}
                rowKey={(record) => record.id}
              >
                <Column
                  render={(record: any) => (
                    <>
                      <p key={record.id}>
                        <span>{record.name}</span> 在{' '}
                        <Dropdown
                          overlay={menu}
                          placement="top"
                          arrow={{ pointAtCenter: true }}
                        >
                          <span>文章</span>
                        </Dropdown>{' '}
                        评论{' '}
                        <Popover title={record.content}>
                          <span> 查看内容</span>
                        </Popover>{' '}
                        {record.pass ? (
                          <Badge status="success" text="通过" />
                        ) : (
                          <Badge status="error" text="未通过" />
                        )}
                      </p>
                    </>
                  )}
                />
                <Column
                  key="action"
                  align="right"
                  render={(_: any, record: DataType) => (
                    <Space size="middle">
                      <span
                        onClick={() => {
                          TongBtn(record.id, record.pass);
                        }}
                      >
                        通过
                      </span>
                      <span
                        onClick={() => {
                          JuBtn(record.id, record.pass);
                        }}
                      >
                        拒绝
                      </span>
                      <span
                        onClick={() => {
                          setIsShowModal(true);
                          setCommentInfo({
                            email: userInfo.email
                              ? userInfo.email
                              : '3160721510@qq.com',
                            hostId: record.hostId,
                            name: userInfo.name,
                            parentCommentId: record.id,
                            replyUserEmail: record.email
                              ? record.email
                              : '3160721510@qq.com',
                            replyUserName: record.name,
                            url: record.url,
                          });
                        }}
                      >
                        回复
                      </span>
                      <Popconfirm
                        title="您确定要删除吗?"
                        onConfirm={() => {
                          confirm(record.id);
                        }}
                        onCancel={() => cancel}
                        okText="Yes"
                        cancelText="No"
                      >
                        <span>删除</span>
                      </Popconfirm>
                    </Space>
                  )}
                />
              </Table>
            </div>
            <BaseModal
              isShowModal={isShowModal}
              clickCancel={() => {
                setIsShowModal(false);
              }}
              clickReply={handleReply}
              setIsModalOpen={false}
            />
          </div>
          {/* 底部盒子 */}
          <div className={style.BottomBox}>
            <div className={style.footerGithubWrap}>
              <div>
                <GithubOutlined
                  className="footer-github"
                  onClick={() => (window.location.href = 'https://github.com/')}
                />
              </div>
              <div>
                <span>
                  Copyright
                  <CopyrightOutlined className="footer-CopyrightOutlined" />
                  2022 Designed by Fantasticit.
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </PageHeaderWrapper>
  );
};

export default Workbench;
