import { EllipsisOutlined, PlusOutlined } from '@ant-design/icons';
import type { ActionType, ProColumns } from '@ant-design/pro-components';
import { ProTable, TableDropdown } from '@ant-design/pro-components';
import {
  Popover,
  Button,
  Badge,
  Modal,
  Input,
  Popconfirm,
  message,
} from 'antd';
import { useRef, useState } from 'react';
import request from 'umi-request';
import { useRequest } from 'ahooks';
import { commentData, editComment, deletComment } from '@/api/comment/index';
//引入样式
import './index.less';
// import { await } from '@umijs/deps/compiled/signale';
// import { await } from '@umijs/deps/compiled/signale';

const { TextArea } = Input;

type GithubIssueItem = {
  url: string;
  id: number;
  number: number;
  title: string;
  labels: {
    name: string;
    color: string;
  }[];
  state: string;
  comments: number;
  created_at: string;
  updated_at: string;
  closed_at?: string;
};

// //获取数据
// const data =  useRequest(commentData)
// console.log(data,'dfsdfsdf');
//获取数据
// const { data: data } = useRequest(commentData);
// console.log(data, '11111111111111111111');

// const actionRef = useRef<ActionType>();

export default () => {
  //解构出来数据
  // const {data:data} = useRequest(commentData);
  // console.log(data.data[0], 'dfsdfsdf--------------');
  //已经页面没有点击，
  const { runAsync } = useRequest(commentData, {
    // runAsync 是一个返回 Promise 的异步函数，如果使用 runAsync 来调用，则意味着你需要自己捕获异常
    manual: true, // 打开请求模式
  });
  // console.log(runAsync,'runAsyncrunAsyncrunAsync');

  const actionRef = useRef<ActionType>();

  const columns: ProColumns<GithubIssueItem>[] = [
    {
      title: '状态',
      width: 200,
      fixed: 'left',
      render: (record: any) => (
        <span>
          {record.pass ? (
            <Badge status="success" />
          ) : (
            <Badge status="warning" />
          )}
          {record.pass ? '通过' : '未通过'}
        </span>
      ),
    },
    {
      title: '称呼',
      dataIndex: 'name',
      width: 200,
      // align:'center'
    },

    {
      title: '联系方式',
      dataIndex: 'email',
      width: 200,
      search: false,
    },
    {
      title: '原始内容',
      dataIndex: 'content',
      width: 200,
      search: false,
      render: (_, record: any) => (
        <Popover content={record.content} title="评论详情-原始内容">
          <a style={{ color: '#0188fb' }}>查看内容</a>
        </Popover>
      ),
    },
    {
      title: 'HTML内容',
      dataIndex: 'html',
      width: 200,
      search: false,
      render: (_, record: any) => (
        <Popover content={record.content} title="评论详情-HTML内容">
          <a style={{ color: '#0188fb' }}>查看内容</a>
        </Popover>
      ),
    },
    {
      title: '管理内容',
      dataIndex: 'content',
      width: 200,
      search: false,
      render: (_, record: any) => (
        <Popover
          arrowPointAtCenter={true}
          placement="right"
          content={
            <div className="content_div">
              <p className="content_p">
                <span className="content_one">404</span>
                <span className="content_three">|</span>
                <span className="content_two">
                  This page could not be found
                </span>
              </p>
            </div>
          }
          title="页面预览"
        >
          <a style={{ color: '#0188fb' }}>文章</a>
        </Popover>
      ),
    },
    {
      title: '创建时间',
      dataIndex: 'updateAt',
      width: 200,
      //在搜索框上面不显示的意思
      search: false,
    },
    {
      title: '父级评论',
      dataIndex: 'userAgent',
      width: 200,
      search: false,
    },

    {
      title: '操作',
      valueType: 'option',
      key: 'option',
      width: 300,
      fixed: 'right',
      render: (text: any, record: any, _, action) => [
        <a
          style={{ color: '#0188fb' }}
          key="editable"
          onClick={async () => {
            // 前端数据的改变
            record.pass = true;
            // 进行后端数据状态的更改
            editComment(record);
            // 刷新页面
            actionRef.current && actionRef.current.reload();
          }}
        >
          通过
        </a>,
        <a
          style={{ color: '#0188fb' }}
          onClick={async () => {
            record.pass = false;
            // 进行后端数据状态的更改
            editComment(record);
            actionRef.current && actionRef.current.reload();
          }}
        >
          拒绝
        </a>,
        <a style={{ color: '#0188fb' }} onClick={showModal}>
          回复
        </a>,
        <div style={{ color: '#0188fb' }} onClick={() => {}}>
          <Popconfirm
            title="确认删除这个评论？"
            // 成功
            onConfirm={() => {
              message.success('Click on Yes');
              deletComment(record.id);
              actionRef.current && actionRef.current.reload();
            }}
            onCancel={() => message.error('Click on No')}
            okText="确认"
            cancelText="取消"
          >
            <a style={{ color: '#0188fb' }}> 删除</a>
          </Popconfirm>
        </div>,
      ],
    },
  ];

  //设置点击分页的页数
  const [page, setPage] = useState(1);

  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  return (
    <>
      <ProTable<GithubIssueItem>
        scroll={{ x: 1900 }}
        columns={columns}
        actionRef={actionRef}
        cardBordered
        //复选框
        rowSelection={{
          type: 'checkbox',
        }}
        //加载数据
        request={async (options) => {
          options = {
            ...options,
            page: options.current,
          };
          delete options.current;

          const { data } = await runAsync(options);
          return {
            data: data[0],
            success: true,
            total: data[1],
          };
        }}
        editable={{
          type: 'multiple',
        }}
        columnsState={{
          persistenceKey: 'pro-table-singe-demos',
          persistenceType: 'localStorage',
          onChange(value) {
            console.log('value: ', value);
          },
        }}
        rowKey="id"
        search={{
          labelWidth: 'auto',
          defaultCollapsed: false,
          collapseRender: () => '',
        }}
        options={{
          setting: {
            listsHeight: 400,
          },
        }}
        form={{
          // 由于配置了 transform，提交的参与与定义的不同这里需要转化一下
          syncToUrl: (values, type) => {
            if (type === 'get') {
              return {
                ...values,
                created_at: [values.startTime, values.endTime],
              };
            }
            return values;
          },
        }}
        //分页
        pagination={{
          current: page,
          pageSize: 12,
          onChange: (page) => {
            console.log(page, 'page-----------------');
            setPage(page);
            // 当前页面页数的更改并且重新加载页面
            actionRef.current && actionRef.current.reload();
          },
        }}
        dateFormatter="string"
      />
      <Modal
        title="回复评论"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <TextArea rows={4} placeholder="支持Markdown" />
      </Modal>
    </>
  );
};
