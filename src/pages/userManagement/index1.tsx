import { FC } from 'react';
import { Badge } from 'antd';
import { useRef } from 'react';
import style from './index.less';
import { useRequest } from 'ahooks';
import { GetUserData } from '@/api/userApi';
import { ProTable } from '@ant-design/pro-components';
import type { ActionType } from '@ant-design/pro-components';

const UserTable: FC = () => {
  const actionRef = useRef<ActionType | undefined>();

  const { runAsync } = useRequest(GetUserData, {
    manual: true, // 打开请求模式 runAsync 是一个返回 Promise 的异步函数，如果使用 runAsync 来调用，则意味着你需要自己捕获异常
  });

  const columns: any = [
    {
      title: '账户',
      dataIndex: 'name',
    },
    {
      title: '邮箱',
      dataIndex: 'email',
    },
    {
      title: '角色',
      tooltip: 'EllipsisOutlined',
      render: (record: any) => (
        <span>{record.role == 'admin' ? '管理员' : '访客'}</span>
      ),
    },
    {
      title: '状态',
      render: (record: any) => (
        <span>
          {record.status == 'active' ? (
            <Badge status="success" />
          ) : (
            <Badge status="warning" />
          )}
          {record.status == 'active' ? '可用' : '已锁定'}
        </span>
      ),
    },
    {
      title: '注册日期',
      dataIndex: 'createAt',
      hideInSearch: true,
    },
    {
      title: '操作',
      render: (record: any) => {
        return (
          <span>
            {record.status == 'active' ? <a>禁用</a> : <a>启用</a>}
            &emsp;
            {record.role == 'admin' ? <a>解除授权</a> : <a>授权</a>}
          </span>
        );
      },
      hideInSearch: true,
    },
  ];

  return (
    <div>
      <ProTable
        // 批量操作
        className={style.UserManagementWrap_Table}
        rowSelection={{
          type: 'checkbox',
        }}
        rowKey={'id'}
        columns={columns}
        actionRef={actionRef}
        pagination={{
          current: 1,
          pageSize: 10,
        }}
        search={{
          searchText: '查询',
          span: 6,
          //   collapsed: false,
          collapseRender: () => <div></div>,
          optionRender: (searchConfig, formProps, dom) => {
            return dom;
          },
        }}
        request={
          // 初始化的时候创建表格执行
          // 分页 改变执行,
          // 查询的时候改变执行
          async (options, sort, filter) => {
            options = {
              ...options,
              page: options.current,
              pageSize: options.pageSize,
            };
            delete options.current;
            // 手动发起 GetUserData 请求;
            const { data } = await runAsync(options); // 手动发起请求
            return Promise.resolve({
              total: data[1],
              data: data[0],
              success: true,
            });
          }
        }
      ></ProTable>
    </div>
  );
};

export default UserTable;
