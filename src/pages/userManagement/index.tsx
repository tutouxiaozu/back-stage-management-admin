import style from './index.less';
import { Button, Table, Badge } from 'antd';
import { FC, useState, useEffect } from 'react';
import UserPagination from '@/components/userPagination';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { GetUserData, operationStatus } from '@/api/userApi';
import useBreadRoutes from '@/hooks/useBreadcrumb/useBreadcrumb';
import {
  ProFormText,
  QueryFilter,
  ProFormSelect,
} from '@ant-design/pro-components';

const breadcrumbRoutes = [
  { path: '/workbench', breadcrumbName: '工作台' },
  { path: '/searchRecord', breadcrumbName: '用户管理' },
];

const UserTable: FC = () => {
  const [page, setPage] = useState(1);
  const [data, setData] = useState([]);
  const [name, setName] = useState('');
  const [role, setRole] = useState('');
  const [total, setTotal] = useState(1);
  const [email, setEmail] = useState('');
  const [status, setStatus] = useState('');
  const [pageSize, setPageSize] = useState(10);
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);

  const GetUserTableData = () => {
    GetUserData({
      page,
      name,
      role,
      email,
      status,
      pageSize,
    }).then((res) => {
      let arr = res.data[0];
      arr.forEach((item: any) => (item.key = item.id));
      setData(arr);
      setTotal(res.data[1]);
    });
  };

  // 点击 多选的 启动 解除权限等按钮；
  const AllStart = async (selectedRowKeys: any, str?: string) => {
    let list: any = [];
    data.forEach((item: any) => {
      selectedRowKeys.forEach((val: any) => {
        if (item.id === val) {
          list.push(item);
        }
      });
    });

    if (list.length > 0) {
      await list.forEach((_: any) => {
        if (str === 'active' || str === 'locked') {
          _.status = str;
          setTimeout(() => {
            operationStatus(_);
          }, 300);
        } else if (str === 'admin' || str === 'visitor') {
          _.role = str;
          setTimeout(() => {
            operationStatus(_);
          }, 300);
        }
      });
      setTimeout(() => {
        GetUserTableData();
      }, 1000);
    } else {
      return;
    }
  };

  useEffect(() => {
    GetUserTableData();
  }, [page, pageSize, total]);

  // 表格点击删除事件
  const start = async (str: string) => {
    AllStart(selectedRowKeys, str);
    setTimeout(() => {
      // 实现一个 异步 完成事件，相当于loading。。。
      setSelectedRowKeys([]);
    }, 1000);
  };

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  // 当数据 没有选中的时候 就是禁用状态
  const hasSelected = selectedRowKeys.length > 0;

  const columns: any = [
    {
      title: '账户',
      dataIndex: 'name',
    },
    {
      title: '邮箱',
      dataIndex: 'email',
    },
    {
      title: '角色',
      tooltip: 'EllipsisOutlined',
      render: (record: any) => (
        <span>{record.role == 'admin' ? '管理员' : '访客'}</span>
      ),
    },
    {
      title: '状态',
      render: (record: any) => (
        <span>
          {record.status == 'active' ? (
            <Badge status="success" />
          ) : (
            <Badge status="warning" />
          )}
          {record.status == 'active' ? '可用' : '已锁定'}
        </span>
      ),
    },
    {
      title: '注册日期',
      hideInSearch: true,
      dataIndex: 'createAt',
    },
    {
      title: '操作',
      render: (record: any, _: any) => {
        return (
          <span>
            <a
              className={style.UserManagementWrap_a_style}
              onClick={() => {
                let status = record.status == 'active' ? 'locked' : 'active';
                _.status = status;
                (async () => {
                  await operationStatus(_);
                  GetUserTableData();
                })();
              }}
            >
              {record.status == 'active' ? '禁用' : '启用'}
            </a>
            &emsp;
            <a
              className={style.UserManagementWrap_a_style}
              onClick={() => {
                let role = record.role == 'admin' ? 'visitor' : 'admin';
                _.role = role;
                (async () => {
                  await operationStatus(_);
                  GetUserTableData();
                })();
              }}
            >
              {record.role == 'admin' ? '解除授权' : '授权'}
            </a>
          </span>
        );
      },
      hideInSearch: true,
    },
  ];

  const QueryFilterOnFinish = (values: any) => {
    setName(values.name);
    setRole(values.role);
    setEmail(values.email);
    setStatus(values.status);
    GetUserTableData();
  };

  const PagChange = (props: any) => {
    let { page, pageSize } = props;
    setPage(page);
    setPageSize(pageSize);
  };

  return (
    <PageHeaderWrapper
      title={false}
      breadcrumb={useBreadRoutes(breadcrumbRoutes)}
    >
      <div className={style.UserManagementWrap}>
        <div className={style.UserManagementWrap_header}>
          {/* 搜索 部分的组件拆分 */}
          <QueryFilter
            defaultCollapsed
            split
            span={4}
            onFinish={async (values) => {
              console.log(
                values,
                '----------------------------------------------------------------',
              );
              setName(values.name);
              setRole(values.role);
              setEmail(values.email);
              setStatus(values.status);
              await GetUserTableData();
            }}
          >
            <ProFormText name="name" label="账户" placeholder={'请输入账户'} />
            <ProFormText name="email" label="邮箱" placeholder={'请输入邮箱'} />
            <ProFormSelect
              name="role"
              label="角色"
              showSearch
              valueEnum={{
                admin: '管理员',
                visitor: '访客',
              }}
            />
            <ProFormSelect
              name="status"
              label="状态"
              showSearch
              valueEnum={{
                locked: '锁定',
                active: '可用',
              }}
            />
          </QueryFilter>
        </div>

        <div className={style.UserManagementWrap_Table}>
          {/* 数据展示表格 */}
          <div
            style={{ marginBottom: 16 }}
            className={!hasSelected ? style.UserManagementWrap_checkbox : ''}
          >
            <Button onClick={() => start('active')}>启动</Button>
            &emsp;
            <Button onClick={() => start('locked')}>禁用</Button>
            &emsp;
            <Button onClick={() => start('visitor')}>解除授权</Button>
            &emsp;
            <Button onClick={() => start('admin')}>授权</Button>
          </div>
          <Table
            rowSelection={rowSelection}
            columns={columns}
            dataSource={data}
            pagination={false}
          />
          {/* 分页组件的拆分 */}
          <UserPagination children={{ total, page, pageSize, PagChange }} />
        </div>
      </div>
    </PageHeaderWrapper>
  );
};

export default UserTable;
