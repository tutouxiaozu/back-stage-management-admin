//引入样式
import './index.less';
import type { UploadProps } from 'antd';
import { message, Upload, Table } from 'antd';
import { FC, useState, useEffect } from 'react';
import { GetPosterData } from '@/api/postrtApi';
import { InboxOutlined } from '@ant-design/icons';
import UserPagination from '@/components/userPagination';
import { ProFormText, QueryFilter } from '@ant-design/pro-components';

const { Dragger } = Upload;

//上传OSS
const props: UploadProps = {
  name: 'file',
  multiple: true,
  action: 'yangyang0302.oss-cn-beijing.aliyuncs.com',
  onChange(info) {
    const { status } = info.file;
    if (status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (status === 'done') {
      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
  onDrop(e) {
    console.log('Dropped files', e.dataTransfer.files);
  },
};
// 表格

const columns: any = [];

// 海报管理页面
const Poster: FC = () => {
  // className={style.PosterWrap}
  const [page, setPage] = useState(1);
  const [data, setData] = useState([]);
  const [total, setTotal] = useState(1);
  const [pageSize, setPageSize] = useState(10);

  const GetUserTableData = async () => {
    await GetPosterData({
      page,
      pageSize,
    }).then((res) => {
      let arr = res.data[0];
      arr.forEach((item: any) => (item.key = item.id));
      setData(arr);
      setTotal(res.data[1]);
    });
  };

  useEffect(() => {
    GetUserTableData();
  }, [page, pageSize, total]);

  const PagChange = (props: any) => {
    let { page, pageSize } = props;
    setPage(page);
    setPageSize(pageSize);
  };

  return (
    <div
      style={{
        padding: 24,
      }}
    >
      <Dragger {...props}>
        <p className="ant-upload-drag-icon">
          <InboxOutlined />
        </p>
        <p className="ant-upload-text">点击选择文件或将文件拖拽到此处</p>
        <p className="ant-upload-hint">文件将上传到OSS，如未配置请先配置</p>
      </Dragger>
      {/* 上面的搜索选区 */}
      <QueryFilter<{
        name: string;
      }>
        //搜索案件的功能
        onFinish={async (values) => {
          GetUserTableData();
        }}
      >
        <ProFormText
          name="creater"
          label="文件名称"
          placeholder="请输入文件名称"
        />
      </QueryFilter>
      <Table dataSource={data} columns={columns} />
      <UserPagination children={{ total, page, pageSize, PagChange }} />
    </div>
  );
};

export default Poster;
