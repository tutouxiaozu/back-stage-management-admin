import { FC, useState } from 'react';
import style from './index.less';
import { GetAccessData, DeleteAccess } from '@/api/accessStatisticsApi';
import { useRequest } from 'ahooks';
import type { ActionType } from '@ant-design/pro-components';
import { ProTable } from '@ant-design/pro-components';
import { Badge, Popconfirm, message } from 'antd';
import { useRef } from 'react';

const AccessStatistics: FC = () => {
  const actionRef = useRef<ActionType | undefined>();
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(5);

  const { run, runAsync } = useRequest(GetAccessData, {
    manual: true, // 打开请求模式
  });

  const columns: any = [
    {
      title: 'URL',
      render: (record: any) => <a>{record.url}</a>,
      width: 200,
      fixed: 'left',
    },
    {
      title: 'IP',
      dataIndex: 'ip',
      width: 200,
    },
    {
      title: '浏览器',
      dataIndex: 'browser',
      width: 200,
    },
    {
      title: '内核',
      dataIndex: 'engine',
      width: 200,
    },
    {
      title: '操作系统',
      dataIndex: 'os',
      width: 200,
    },
    {
      title: '设备',
      dataIndex: 'device',
      width: 200,
    },
    {
      title: '地址',
      dataIndex: 'address',
      width: 200,

      ellipsis: true,
    },
    {
      title: '访问量',
      dataIndex: 'count',
      width: 200,
    },
    {
      title: '访问时间',
      dataIndex: 'updateAt',
      width: 200,
    },
    {
      title: '操作',
      fixed: 'right',
      width: 100,
      render: (record: any) => {
        return (
          <Popconfirm
            title="您确定删除该数据?"
            onConfirm={() => confirm(record)}
            onCancel={() => cancel()}
            okText="Yes"
            cancelText="No"
          >
            <a>删除</a>
          </Popconfirm>
        );
      },
    },
  ];

  // 删除 单项数据的时候
  const confirm = async (e: any) => {
    console.log(e, '--------------------------------');
    // await DeleteAccess(e.id).then(res=>{
    //   console.log(res,'--------------------------------');
    // })
    run({
      page,
      pageSize,
    });
    message.success('删除成功');
  };

  const cancel = () => {
    message.error('取消删除');
  };

  return (
    <div>
      <ProTable
        // 批量操作
        className={style.UserManagementWrap_Table}
        rowSelection={{
          type: 'checkbox',
        }}
        actionRef={actionRef}
        columns={columns}
        rowKey={'id'}
        pagination={{
          current: 1,
          pageSize: 5,
        }}
        search={{
          searchText: '查询',
          span: 4,
          //   collapsed: false,
          collapseRender: () => <div></div>,
          optionRender: (searchConfig, formProps, dom) => {
            return dom;
          },
        }}
        request={
          // 初始化的时候创建表格执行
          // 分页 改变执行,
          // 查询的时候改变执行
          async (options, sort, filter) => {
            options = {
              ...options,
              page: options.current,
              pageSize: options.pageSize,
            };
            delete options.current;
            // 手动发起 GetUserData 请求;
            const { data } = await runAsync(options); // 手动发起请求
            return Promise.resolve({
              total: data[1],
              data: data[0],
              success: true,
            });
          }
        }
      ></ProTable>
    </div>
  );
};

export default AccessStatistics;
