import style from './index.less';
import { useRequest } from 'ahooks';
import { FC, useState, useEffect } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import useBreadRoutes from '@/hooks/useBreadcrumb/useBreadcrumb';
import { ProFormText, QueryFilter } from '@ant-design/pro-components';
import { GetAccessData, DeleteAccess } from '@/api/accessStatisticsApi';
import { Button, Table, Space, Pagination, message, Popconfirm } from 'antd';
import UserPagination from '@/components/userPagination';

const breadcrumbRoutes = [
  { path: '/workbench', breadcrumbName: '工作台' },
  { path: '/accessStatistics', breadcrumbName: '访问统计' },
];

// 访问记录页面
const AccessStatistics: FC = () => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(5);
  const [total, setTotal] = useState(0);
  const [list, setList] = useState([]);
  const { run, runAsync } = useRequest(GetAccessData, {
    manual: true,
  });
  const [ip, setip] = useState('');
  const [userAgent, setuserAgent] = useState('');
  const [url, seturl] = useState('');
  const [address, setaddress] = useState('');
  const [browser, setbrowser] = useState('');
  const [engine, setengine] = useState('');
  const [os, setos] = useState('');
  const [device, setdevice] = useState('');

  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const [loading, setLoading] = useState(false); // 点击 按钮模拟异步 loading 事件;

  // columns 是表格渲染用的所有的 类型
  const columns: any = [
    {
      title: 'URL',
      render: (record: any) => (
        <a className={style.AccessStatisticsWrap_a_style}>{record.url}</a>
      ),
      width: 200,
      fixed: 'left',
    },
    {
      title: 'IP',
      dataIndex: 'ip',
      width: 200,
    },
    {
      title: '浏览器',
      dataIndex: 'browser',
      width: 200,
    },
    {
      title: '内核',
      dataIndex: 'engine',
      width: 200,
    },
    {
      title: '操作系统',
      dataIndex: 'os',
      width: 200,
    },
    {
      title: '设备',
      dataIndex: 'device',
      width: 200,
    },
    {
      title: '地址',
      dataIndex: 'address',
      width: 200,

      ellipsis: true,
    },
    {
      title: '访问量',
      dataIndex: 'count',
      width: 200,
    },
    {
      title: '访问时间',
      dataIndex: 'updateAt',
      width: 200,
    },
    {
      title: '操作',
      fixed: 'right',
      width: 100,
      render: (record: any) => {
        return (
          <Popconfirm
            title="您确定删除该数据?"
            onConfirm={() => confirm(record)}
            onCancel={() => cancel()}
            okText="Yes"
            cancelText="No"
          >
            <a className={style.AccessStatisticsWrap_a_style}>删除</a>
          </Popconfirm>
        );
      },
    },
  ];

  // 请求数据的方法
  const GetHttpAccessData = async () => {
    const { data }: any = await runAsync({
      page,
      pageSize,
      ip,
      userAgent,
      url,
      address,
      browser,
      engine,
      os,
      device,
    });

    if (data && data !== undefined) {
      let arr = data[0];
      arr.forEach((item: any) => (item.key = item.id));
      setList(arr);
      setTotal(data[1]);
    }
  };

  useEffect(() => {
    GetHttpAccessData();
  }, [page, pageSize, total]);

  // 表格多选点击删除事件
  const start = () => {
    setLoading(true);
    AllDelete(selectedRowKeys);
    setTimeout(() => {
      // selectedRowKeys 这个数组中存放了 所有选中的数据的 ID
      setLoading(false);
      setSelectedRowKeys([]);
    }, 1000);
  };

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  // 当数据 没有选中的时候 就是禁用状态
  const hasSelected = selectedRowKeys.length > 0;

  // 删除 单项数据的时候
  const confirm = async (e: any) => {
    await DeleteAccess({
      id: e.id,
    });
    GetHttpAccessData();
    message.success('删除成功');
  };

  const cancel = () => {
    message.error('取消删除');
  };

  // 删除 选择的 选项
  const AllDelete = async (data: any) => {
    await data.forEach((item: any) => {
      setTimeout(() => {
        DeleteAccess({ id: item }); // 请求删除接口
      }, 500);
    });
    setTimeout(() => {
      GetHttpAccessData(); // 重新请求数据
    }, 1500);
  };

  // 分页事件
  const PagChange = (props: any) => {
    let { page, pageSize } = props;
    setPage(page);
    setPageSize(pageSize);
  };

  return (
    <PageHeaderWrapper
      title={false}
      breadcrumb={useBreadRoutes(breadcrumbRoutes)}
    >
      <div className={style.AccessStatisticsWrap}>
        <div className={style.AccessStatisticsWrap_utils_header}>
          <QueryFilter
            split
            span={6}
            defaultColsNumber={12}
            defaultCollapsed={true}
            onFinish={async (values) => {
              setip(values.type);
              setuserAgent(values.words);
              seturl(values.url);
              setaddress(values.address);
              setbrowser(values.browser);
              setengine(values.kernel);
              setos(values.os);
              setdevice(values.equipment);

              setTimeout(() => {
                GetHttpAccessData(); // 重新请求数据
              }, 1000);
            }}
          >
            <ProFormText name="type" label="IP" placeholder={'请输入IP地址'} />
            <ProFormText
              name="words"
              label="UA"
              placeholder={'请输入User Agent'}
            />
            <ProFormText name="url" label="URL" placeholder={'请输入URL'} />
            <ProFormText
              name="address"
              label="地址"
              placeholder={'请输入地址'}
            />
            <ProFormText
              name="browser"
              label="浏览器"
              placeholder={'请输入浏览器'}
            />
            <ProFormText
              name="kernel"
              label="内核"
              placeholder={'请输入内核'}
            />
            <ProFormText name="os" label="OS" placeholder={'请输入操作系统'} />
            <ProFormText
              name="equipment"
              label="设备"
              placeholder={'请输入设备'}
            />
          </QueryFilter>
        </div>

        <div className={style.AccessStatisticsWrap_list_wrap}>
          {/* 数据展示表格 */}
          {/* <DeleteTable ></DeleteTable> */}
          <div style={{ marginBottom: 16 }}>
            <Button
              danger
              type="primary"
              onClick={start}
              disabled={!hasSelected}
              loading={loading}
            >
              Delete
            </Button>
            <span style={{ marginLeft: 8 }}>
              {hasSelected ? `当前选中 ${selectedRowKeys.length} 记录` : ''}
            </span>
          </div>
          <Table
            scroll={{ x: 500 }}
            rowSelection={rowSelection}
            columns={columns}
            dataSource={list}
            pagination={false}
          />
          <div className={style.AccessStatisticsWrap_Pagination}>
            <UserPagination children={{ total, page, pageSize, PagChange }} />
          </div>
        </div>
      </div>
    </PageHeaderWrapper>
  );
};

export default AccessStatistics;
