import React, { useEffect, useState } from 'react';
import style from './drawer.less';
import './drawer.less';
import { Drawer, Select } from 'antd';
import Forms from '../Froms/index';
import { CloseOutlined } from '@ant-design/icons';
// import { articleManager } from '@/services/article';
// import { useRequest } from 'umi';
// import FileManager from '@/pages/FileManager/index';
type Props = {
  [propsname: string]: any;
};

export default function Drawerindex(props: Props) {
  const { visible, closeDrawer, item, getFormValue } = props;
  const [value, setValue] = useState('');
  const [childrenDrawer, setChildrenDrawer] = useState(false); //控制设置内层弹窗
  //点击关闭弹窗关闭
  const onClose = () => {
    closeDrawer();
  };
  const showChildrenDrawer = (flag: boolean) => {
    setChildrenDrawer(flag);
  };

  const onChildrenDrawerClose = () => {
    setChildrenDrawer(false);
  };
  return (
    <div>
      <Drawer
        title="页面属性"
        width={600}
        closable={false}
        onClose={onClose}
        visible={visible}
        extra={<CloseOutlined onClick={onClose} />}
      >
        <Forms
          showChildrenDrawer={showChildrenDrawer}
          item={item}
          getFormValue={getFormValue}
        />
        <Drawer
          title="文件选择"
          width={800}
          closable={false}
          onClose={onChildrenDrawerClose}
          visible={childrenDrawer}
          extra={<CloseOutlined onClick={onChildrenDrawerClose} />}
        >
          {/* <FileManager type="mdEditPad" /> */}
        </Drawer>
      </Drawer>
    </div>
  );
}
