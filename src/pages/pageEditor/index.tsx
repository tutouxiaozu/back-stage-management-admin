import './index.css';
import React from 'react';
import texts from './Detailtext';
import style from './index.less';
import Drawers from './Drawer/drawer';
import { getPages } from '../../api/Fa';
import { history, useRequest } from 'umi';
import Editors from '@/components/Editors';
import MDEditor from '@uiw/react-md-editor';
import * as http from '../../api/AmEditor/index';
import { EllipsisOutlined } from '@ant-design/icons';
import { FC, useEffect, useRef, useState } from 'react';
import EditorsFrom from '@/components/editorsFrom/editors';
import {
  Upload,
  Dropdown,
  Menu,
  Drawer,
  Modal,
  Button,
  message,
  Popconfirm,
  Pagination,
  Input,
  Alert,
  MenuProps,
} from 'antd';
// 页面管理页面
interface searchType {
  name?: string;
  types?: string;
}
interface MenuDataItem {
  title: string;
  type: string;
  level: number;
  key: number;
}
function amEditor() {
  //渲染----------------------------------------------
  // 列表数据
  let [file, setFile] = useState([]);
  // 页码
  let [page, setPage] = useState(1);
  // 显示页数
  let [pageSize, setPageSize] = useState(12);
  // 总长度
  let [length, setLength] = useState(0);
  // 搜索信息
  let [originname, setOriginname] = useState('');
  let [type, setType] = useState('');
  const [menuData, setMenuData] = useState<MenuDataItem[]>(); //toc加内容
  //发布----------------------------------------------
  const { run } = useRequest(getPages, { manual: true });
  const perview = useRef<any>();
  const id = history.location.query?.id;
  const getInfo = useRequest(getPages, { manual: true });
  const [text, setText] = useState(texts);
  //根据文章id获取的文章/新建的文章
  const [info, setInfo] = useState<any>({});
  const [visible, setVisible] = useState(false);
  //发布----------------------------------------------

  // 获取文件列表
  const getFile = (
    page?: number,
    pageSize?: number,
    originalname?: string,
    type?: string,
  ) => {
    http.getFileList({ page, pageSize, originalname, type }).then((res) => {
      setFile(res.data[0]);
      console.log(res.data[0]);
      setLength(res.data[1]);
    });
  };
  // 获取分页数据
  useEffect(() => {
    getFile(page, pageSize, originname, type);
  }, [page, pageSize, originname, type]);
  const confirm: any = (e: React.MouseEvent<HTMLElement>) => {
    // console.log(e);
    history.push('/workbench');
  };
  const cancel: any = (e: React.MouseEvent<HTMLElement>) => {
    // console.log(e);
    message.error('请保存');
  };
  const handleMenuClick: MenuProps['onClick'] = (e) => {
    // message.info('Click on menu item.');
    console.log('click', e);
  };
  //删除
  const confirmDel = () => {
    Modal.confirm({
      title: '确认删除？',
      content: '删除内容后，无法恢复。',
      transitionName: '',
      maskTransitionName: '',
      onOk() {
        run({
          method: 'delete',
          id,
        }).then(() => {
          message.success('删除文章成功');
          history.replace('/pageManagement');
        });
      },
    });
  };

  //设置-----------------------------------
  //点击设置
  const openDrawer = () => {
    setVisible(true);
  };
  const closeDrawer = () => {
    setVisible(false);
  };
  //点击确定
  // const confirm = () => {
  //   history.push('/ArticleManager/allaticle');
  // };

  const menu = (
    <Menu
      onClick={handleMenuClick}
      items={[
        {
          label: '查看',
          key: '1',
          disabled: id ? false : true,
        },
        {
          label: (
            <a
              onClick={(e) => {
                e.preventDefault();
                if (!info.name) {
                  message.warn('请先设置标题');
                } else {
                  openDrawer();
                }
              }}
            >
              设置
            </a>
          ),
          key: '2',
        },
        {
          label: (
            <a
              onClick={(e) => {
                e.preventDefault();
                if (!info.name) {
                  message.warn('请先设置标题');
                } else {
                  createOrudpArticle('draft');
                }
              }}
            >
              保存草稿
            </a>
          ),
          key: '3',
        },
        {
          label: <span onClick={confirmDel}>删除</span>,
          disabled: id ? false : true,
          key: '4',
        },
      ]}
    />
  );
  //设置-----------------------------------

  //渲染弹框--------------------------------------
  const [open, setOpen] = useState(false);
  const showDrawer = () => {
    setOpen(true);
  };
  const onClose = () => {
    setOpen(false);
  };
  //渲染弹框--------------------------------------

  //编辑器-----------------------------------------
  // const hanleChange = (e: any) => {
  //   setValue(e);
  // };
  const [value, setValue] = useState(() => text);
  //编辑器-----------------------------------------
  const priview = useRef<any>();
  const formatMenuData = () => {
    const previewEl = priview.current.querySelector('.w-md-editor-preview');
    const menuDataEl = Array.from(previewEl.querySelectorAll('*')).filter(
      (item: any) => /^H[1-6]$/.test(item.nodeName),
    );
    setMenuData(
      menuDataEl.map<MenuDataItem>((item: any, index: number) => ({
        title: item.innerText,
        type: item.nodeName,
        level: item.nodeName.slice(1) * 1,
        key: index,
      })),
    );
  };
  useEffect(() => {
    setTimeout(() => {
      if (priview.current && value) {
        formatMenuData();
      }
    }, 0);
  }, [priview, value, info]);
  useEffect(() => {
    if (id) {
      getInfo
        .run({
          method: 'get',
          id: history.location.query?.id,
        })
        .then((res) => {
          setInfo(res);
          document.title = `编辑页面${res.name}`;
        });
    } else {
      document.title = `新建页面`;
    }
  }, []);
  const hanleChange = (e: any) => {
    if (e.bubbles) {
      setInfo({
        ...info,
        name: e.target.value,
      });
    } else {
      setInfo({ ...info, content: e });
    }
  };
  const getFormValue = (value: any) => {
    let tags = '';
    if (typeof value.tags[0] === 'string') {
      tags = value.tags.join(',');
    } else {
      tags = value.tags
        .map((item: any) => {
          return item.id;
        })
        .join(',');
    }
    const category = value.category.id ? value.category.id : value.category;
    setInfo({ ...info, ...value, tags, category });
  };
  const createOrudpArticle = (status: string) => {
    run({
      method: id ? 'patch' : 'post',
      id,
      data: {
        content: text,
        html: perview.current?.childNodes[1].childNodes[1].innerHTML,
        menuData,
        ...info,
        status,
      },
    }).then((res) => {
      message.success('操作成功');
      if (res.id) {
        history.push({
          pathname: '/pageManagement',
          query: {
            id: res.id,
          },
        });
      }
    });
  };
  return (
    <div className={style.PageManagementWrap}>
      <header className={style.PageManagementHeader}>
        <div className={style.PageManagementTitle}>
          <Popconfirm
            title="确认关闭？如果有内容变更,请先保存"
            onConfirm={confirm}
            onCancel={cancel}
            okText="确认"
            cancelText="取消"
          >
            <Button className={style.Button} size="small">
              X
            </Button>
          </Popconfirm>
          <Input
            onChange={hanleChange}
            value={info.name}
            bordered={false}
            className={style.Input}
            placeholder="请输入文章标题"
          />
        </div>
        <div className={style.PageManagementRelease}>
          <Button
            className={style.Buttons}
            type="primary"
            onClick={() => {
              if (!info.name) {
                message.warn('请先设置标题');
              } else {
                createOrudpArticle('publish');
              }
            }}
          >
            发布
          </Button>
          <Dropdown overlay={menu} placement="bottom">
            <Button className={style.Dian}>
              <EllipsisOutlined />
            </Button>
          </Dropdown>
        </div>
      </header>
      <main className={style.PageManagementMain}>
        <header className={style.header}>
          <div className={style.PageManagementLeft}>
            <Button type="primary" className={style.but} onClick={showDrawer}>
              打开文件库
            </Button>
          </div>
          <div className={style.PageManagementRight}>
            <p className={style.p}>
              当前模式:<a>编辑</a>
              <a>退出两栏显示</a>
              <a>大纲</a>
            </p>
          </div>
        </header>
        <div className={style.Div}>
          <div className={style.PageManagementsLeft} data-color-mode="light">
            <MDEditor
              value={info.content || text}
              onChange={hanleChange}
              ref={(val: any) => {
                if (val) {
                  priview.current = val.container;
                }
              }}
            />
          </div>
          <div className={style.PageManagementsRight}>
            <div className={style.li}>
              {menuData &&
                menuData.map((item) => {
                  return (
                    <p className={style.pss} key={item.key}>
                      {item.title}
                    </p>
                  );
                })}
            </div>
          </div>
        </div>
      </main>
      <Drawer
        title="文件选择"
        placement="right"
        onClose={onClose}
        open={open}
        size="large"
      >
        <Alert message="点击卡片复制链接，点击图片查看大图" type="info" />

        <EditorsFrom
          sendSearchInfo={(obj: searchType) => {
            let { name, types } = obj;
            setOriginname(String(name));
            setType(String(types));
          }}
        ></EditorsFrom>
        <Upload
          action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
          directory
        >
          <Button className={style.buttona}>上传文件</Button>
        </Upload>
        <Editors
          file={file}
          reload={() => getFile(page, pageSize, originname, type)}
        ></Editors>
        <Pagination
          className={style.Pagination}
          onChange={(e) => setPage(e)}
          total={length}
          showTotal={(total) => `共${total}条`}
          defaultPageSize={pageSize}
          pageSize={pageSize}
          defaultCurrent={1}
          current={page}
          pageSizeOptions={[8, 12, 24, 36]}
          onShowSizeChange={(current, size) => setPageSize(size)}
        />
      </Drawer>
      <Drawers
        visible={visible}
        closeDrawer={closeDrawer}
        item={info}
        getFormValue={getFormValue}
      />
    </div>
  );
}
export default amEditor;
