import React, { useState, useEffect } from 'react';
import style from './index.less';
import { Button, Form, Input } from 'antd';
type Props = {
  showChildrenDrawer: (flag: boolean) => void;
  [propsname: string]: any;
};
export default function Froms(props: Props) {
  const { showChildrenDrawer, item, getFormValue } = props;
  const [form] = Form.useForm();
  useEffect(() => {
    form.setFieldsValue(item);
  }, []);
  const onFinish = (values: any) => {
    getFormValue(values);
  };
  return (
    <div>
      <Form
        form={form}
        name="validate_other"
        onFinish={onFinish}
        labelCol={{ span: 4 }}
      >
        <Form.Item
          label="封面"
          name="name"
          colon={false}
          style={{ marginBottom: 15 }}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="路径"
          name="path"
          colon={false}
          style={{ marginBottom: 15 }}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="顺序"
          name="order"
          colon={false}
          style={{ marginBottom: 15 }}
        >
          <Input />
        </Form.Item>
        <div className={style.form_box}></div>
        <div className={style.btn_box}>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              确认
            </Button>
          </Form.Item>
        </div>
      </Form>
    </div>
  );
}
