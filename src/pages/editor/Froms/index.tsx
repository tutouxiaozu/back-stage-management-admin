import React, { useState, useEffect } from 'react';
import style from './index.less';
import { Button, Form, Switch, Upload, Input, Select } from 'antd';
// import { InboxOutlined } from '@ant-design/icons';
import { categoryManager, tagsManager } from '../../../api/Fa';
import { useRequest } from 'umi';
const { TextArea } = Input;
const { Option } = Select;
type Props = {
  showChildrenDrawer: (flag: boolean) => void;
  [propsname: string]: any;
};
export default function Froms(props: Props) {
  const categoryData = useRequest(categoryManager, { manual: true });
  const tagsData = useRequest(tagsManager, { manual: true });
  const [cateData, setCate] = useState([]);
  const [tagData, setTag] = useState([]);
  const { showChildrenDrawer, item, getFormValue } = props;
  const [form] = Form.useForm();
  const [value, setValue] = useState('');
  useEffect(() => {
    categoryData.run({ method: 'get' }).then((res) => {
      setCate(res);
    });
    tagsData.run({ method: 'get' }).then((res) => {
      setTag(res);
    });
    form.setFieldsValue(item);
  }, []);
  const remove = () => {
    form.resetFields(['cover']);
  };
  const onFinish = (values: any) => {
    getFormValue(values);
  };
  const openchild = () => {
    showChildrenDrawer(true);
  };
  return (
    <div>
      <Form
        form={form}
        name="validate_other"
        onFinish={onFinish}
        labelCol={{ span: 4 }}
      >
        <Form.Item
          label="文章摘要"
          name="summary"
          colon={false}
          style={{ marginBottom: 15 }}
        >
          <TextArea
            value={value}
            onChange={(e) => setValue(e.target.value)}
            autoSize={{ minRows: 3, maxRows: 5 }}
          />
        </Form.Item>
        <Form.Item
          label="访问密码"
          name="needPassword"
          colon={false}
          style={{ marginBottom: 15 }}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          label="付费查看"
          name="isPay"
          colon={false}
          style={{ marginBottom: 15 }}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          name="isCommentable"
          label="开启评论"
          valuePropName="checked"
          colon={false}
          style={{ marginBottom: 15 }}
        >
          <Switch />
        </Form.Item>
        <Form.Item
          name="isRecommended"
          label="首页推荐"
          valuePropName="checked"
          colon={false}
          style={{ marginBottom: 15 }}
        >
          <Switch />
        </Form.Item>

        <Form.Item label="选择分类" name="category">
          <Select>
            {cateData.map((item: any) => {
              return (
                <Option key={item.id} value={item.id}>
                  {item.label}
                </Option>
              );
            })}
          </Select>
        </Form.Item>
        <Form.Item name="tags" label="选择标签">
          <Select mode="multiple" placeholder="请选择标签">
            {tagData.map((item: any) => {
              return (
                <Option key={item.id} value={item.id}>
                  {item.label}
                </Option>
              );
            })}
          </Select>
        </Form.Item>
        <Form.Item label="文章封面" colon={false} style={{ marginBottom: 15 }}>
          <div className={style.img}>
            <img
              src={item.cover ? item.cover : './'}
              alt="预览图"
              onClick={openchild}
            />
          </div>
        </Form.Item>
        <Form.Item label=" " name="cover" colon={false}>
          <Input placeholder="或输入外部链接" />
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 3 }} style={{ marginBottom: 15 }}>
          <Button style={{ marginLeft: 15 }} onClick={remove}>
            移除
          </Button>
        </Form.Item>
        <div className={style.form_box}></div>
        <div className={style.btn_box}>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              确认
            </Button>
          </Form.Item>
        </div>
      </Form>
    </div>
  );
}
