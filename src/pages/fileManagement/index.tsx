import { FC, useState, useEffect, SetStateAction } from 'react';
import style from './index.less';
import { useRequest } from 'ahooks';
import * as http from '../../api/file/index';
import BaseBread from '../../components/baseBread/bread';
import type { UploadProps } from 'antd';
import { InboxOutlined } from '@ant-design/icons';
import { Card, Drawer, Image, Popconfirm, Button, Upload } from 'antd';
import { message, Pagination } from 'antd';
import { ProFormText, QueryFilter } from '@ant-design/pro-components';
import type { DrawerProps } from 'antd/es/drawer';
import { GithubOutlined, CopyrightOutlined } from '@ant-design/icons';
const { Dragger } = Upload;
const gridStyle: React.CSSProperties = {
  width: '16.5%',
  textAlign: 'center',
};

// 文件管理页面

const FileManagement: FC = () => {
  const [MoarkHtml, setMoarkHtml] = useState(''); //html数据
  const [MoarkImg, setMoarkImg] = useState([]); //分页数据
  const [page, setPage] = useState(1); //记录页码
  const [pageSize, setPageSize] = useState(12); ///分页
  const [total, setTotal] = useState(1); //分页
  const [fire, setFire] = useState<any>({}); //右侧弹框显示内容
  const [visible, setVisible] = useState(false); //右侧弹框按钮
  const [type, setType] = useState();
  const [originalname, setOriginalname] = useState();
  const [size, setSize] = useState(700);

  //渲染toc数据------------记录页码--每页数据个数--文件名称--文件类型---------------------
  const fileData = async ({ page, pageSize, originalname, type }: any) => {
    let res = await http._file_({ page, pageSize, originalname, type });
    // console.log(res);//返回回来的参数
    setMoarkImg(res.data[0]); //总数据
    setTotal(res.data[1]); //总数据条数
  };
  //
  useEffect(() => {
    fileData({ page, pageSize });
  }, []);
  const onFinish = async ({ originalname, type }: any) => {
    // console.log(values);
    setOriginalname(originalname);
    setType(type);
    await fileData({ originalname, type });
  };

  const showDefaultDrawer = () => {
    // setSize('default');
    setVisible(false);
  };

  const showLargeDrawer = (item: any) => {
    setFire(item);
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };

  const props: UploadProps = {
    name: 'file',
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    headers: {
      authorization: 'authorization-text',
    },
    onChange(info: any) {
      if (info.file.status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === 'done') {
        message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };
  return (
    <div className={style.FileManagementWrap}>
      <div className={style.FileHeader}>
        {/* 面包屑 */}
        <BaseBread></BaseBread>
      </div>
      <div className={style.FileWrap}>
        <div className={style.FileHeaderWrap}>
          <QueryFilter
            defaultCollapsed={false}
            defaultColsNumber={2}
            onFinish={onFinish}
          >
            <ProFormText name="originalname" label="文件名称" />
            <ProFormText name="type" label="文件类型" />
          </QueryFilter>
        </div>
        <div className={style.FileMainWrap}>
          <Dragger
            {...props}
            style={{
              width: '96%',
              marginLeft: '2%',
              background: '#fff',
              marginTop: '25px',
            }}
          >
            <p className="ant-upload-drag-icon">
              <InboxOutlined />
            </p>
            <p className="ant-upload-text">点击选择文件或将文件拖拽到此处</p>
            <p className="ant-upload-hint">
              文件将上传到 OSS, 如未配置请先配置
            </p>
          </Dragger>
          <div className={style.CardWrap}>
            <Card title="Card Title" className={style.CardBox}>
              {MoarkImg &&
                MoarkImg.map((item: any) => {
                  return (
                    <Card.Grid
                      className={style.grid}
                      style={gridStyle}
                      key={item.id}
                    >
                      <div className={style.ImageBox}>
                        <img
                          src={item.url}
                          alt=""
                          onClick={() => {
                            showLargeDrawer(item);
                          }}
                        />
                      </div>
                      <p className={style.title}>{item.originalname}</p>
                    </Card.Grid>
                  );
                })}
              <div className={style.PaginWrap}>
                <Pagination
                  className={style.Pagination}
                  total={total}
                  showSizeChanger
                  hideOnSinglePage={true}
                  current={page}
                  pageSize={pageSize}
                  pageSizeOptions={[5, 10, 15, 20]}
                  responsive={false}
                  showTotal={(total: any) => `共 ${total} 条`}
                  onChange={(
                    page: SetStateAction<number>,
                    pageSize: SetStateAction<number>,
                  ) => {
                    setPage(page);
                    setPageSize(pageSize);
                    fileData({ page, pageSize, originalname, type });
                  }}
                />
              </div>
            </Card>
          </div>
          <div className={style.footerWrap}>
            <div className={style.footerGithubWrap}>
              <div>
                <GithubOutlined
                  className="footer-github"
                  onClick={() => (window.location.href = 'https://github.com/')}
                />
              </div>
              <div>
                <span>
                  Copyright
                  <CopyrightOutlined className="footer-CopyrightOutlined" />
                  2022 Designed by Fantasticit.
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <>
        {/* 右侧抽屉 */}
        <Drawer
          title="文件信息"
          placement="right"
          onClose={onClose}
          visible={visible}
        >
          <Image
            width={592}
            height={360}
            // preview={false}
            // fallback=""
            // @ts-ignore
            src={fire.url}
          />
          <div className="one">
            <span>文件名称:</span>
            {/* @ts-ignore */}
            <span>{fire.originalname}</span>
          </div>
          <div className="two">
            <span>存储路径:</span>
            {/* @ts-ignore */}
            <span>{fire.filename}</span>
          </div>
          <div className="three">
            <p>
              <span>文件类型:</span>
              {/* @ts-ignore */}
              <span>{fire.type}</span>
            </p>
            <p className="four">
              <span>文件大小:</span>
              {/* @ts-ignore */}
              <span> {fire.size}KB</span>
            </p>
          </div>
          <div className="five">
            <span className="le">访问连接:</span>
            <span className="ri">
              {/* @ts-ignore */}
              <span className="t">{fire.url}</span>
              <a
                className="b"
                onClick={() => message.success('内容已复制到剪切板文件信息')}
              >
                复制
              </a>
            </span>
          </div>
          <div className="foot">
            <span></span>
            <div className="right">
              <Button onClick={onClose}>关闭</Button>
              <Popconfirm
                placement="topRight"
                title="Are you sure ？"
                onConfirm={() => {
                  deleteUrl(fire);
                }}
                okText="Yes"
                cancelText="No"
              >
                <Button>删除</Button>
              </Popconfirm>
              {/* <Popconfirm
                title="Are you sure ？"
                key="editable"
              
                okText="确认"
                cancelText="取消"
              >
                <Button danger></Button>
              </Popconfirm> */}
            </div>
          </div>
        </Drawer>
      </>
    </div>
  );
};
export default FileManagement;
// 气泡确认框
function deleteUrl(fire: never[]) {
  throw new Error('Function not implemented.');
}
