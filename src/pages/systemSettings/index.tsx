import { Tabs } from 'antd';
import style from './index.less';
import { FC, useState, useEffect } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { GetSystemSettingsData } from '@/api/systemSettingsApi';
import SetSetup from '@/components/systemSettingsItem/seoSetUp';
import useBreadRoutes from '@/hooks/useBreadcrumb/useBreadcrumb';
import OssSystem from '@/components/systemSettingsItem/ossSystem';
import SystemItem from '@/components/systemSettingsItem/systemItem';
import SMPTService from '@/components/systemSettingsItem/SMPTService';
import DataStatistics from '@/components/systemSettingsItem/dataStatistics';
import InternationalizationItem from '@/components/systemSettingsItem/internationalizationItem';

const breadcrumbRoutes = [
  { path: '/workbench', breadcrumbName: '工作台' },
  { path: '/systemSettings', breadcrumbName: '系统管理' },
];

// 系统设置页面
const SystemSettings: FC = () => {
  const [childrenData, setChildrenData] = useState({});

  useEffect(() => {
    GetSystemSettingsData().then((res) => {
      setChildrenData(res.data);
    });
  }, []);

  const TabNavData = [
    {
      key: '1',
      label: '系统设置',
      children: <SystemItem children={childrenData} />,
    },
    {
      key: '2',
      label: '国际化设置',
      children: <InternationalizationItem children={{ childrenData }} />,
    },
    {
      key: '3',
      label: 'SEO设置',
      children: <SetSetup children={childrenData} />,
    },
    {
      key: '4',
      label: '数据统计',
      children: <DataStatistics children={childrenData} />,
    },
    {
      key: '5',
      label: 'OSS设置',
      children: <OssSystem children={{ childrenData }} />,
    },
    {
      key: '6',
      label: 'SMTP服务',
      children: <SMPTService children={childrenData} />,
    },
  ];

  return (
    <PageHeaderWrapper
      title={false}
      breadcrumb={useBreadRoutes(breadcrumbRoutes)}
    >
      <div className={style.SystemSettingsWrap}>
        <Tabs
          items={TabNavData}
          tabPosition={'left'}
          className={style.SystemSettingsWrap_Table}
        ></Tabs>
      </div>
    </PageHeaderWrapper>
  );
};

export default SystemSettings;
