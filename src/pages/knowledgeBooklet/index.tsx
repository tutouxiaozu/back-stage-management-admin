import React, { useState, useRef, useEffect } from 'react';
import type { ActionType, ProColumns } from '@ant-design/pro-components';
import {
  ProFormSelect,
  ProFormText,
  QueryFilter,
} from '@ant-design/pro-components';
import {
  EditOutlined,
  SettingOutlined,
  CloudUploadOutlined,
  DeleteOutlined,
  CloudDownloadOutlined,
} from '@ant-design/icons';
//引入样式
import './index.less';
import { ProCard } from '@ant-design/pro-components';
import { Card, Button, message, Popconfirm, Tooltip } from 'antd';
import UserPagination from '@/components/userPagination';

//引入数据
import { getKnow } from '@/api/knowledgeBooklet/index';
import { useRequest } from 'ahooks';
//跳转页面
import { history } from 'umi';
//引入抽离的弹框组件
import Drawers from '../../components/drawer/index';

import { delteKnow } from '@/api/knowledgeBooklet/index';

const { Meta } = Card;

export default () => {
  const actionRef = useRef<ActionType>();

  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(1);
  const [data, setData] = useState<any>([]);
  const [pageSize, setPageSize] = useState(6);
  const [status, setstatus] = useState('');
  const [title, settitle] = useState('');

  const [str, setstr] = useState('新建知识库');
  const [open, setOpen] = useState(false);

  //点击当前页面传入的数据
  const [val, setVal] = useState('');
  const onClose = () => {
    setOpen(false);
  };
  const showDrawer = () => {
    // 新建事件
    setstr('新建知识库');
    setOpen(true);
  };
  const showDrawers = () => {
    // 点击设置
    setstr('更新知识库');
    setOpen(true);
  };

  //获取数据
  // 变为手动模式
  const { runAsync } = useRequest(getKnow, {
    manual: true,
  });

  // 请求数据的方法
  const GetKnowledgeData = () => {
    // ()()自己调用自己
    (async () => {
      let arr = await runAsync({
        page,
        pageSize,
        status,
        title,
      });
      setData(arr.data[0]);
      setTotal(arr.data[1]);
    })();
  };

  useEffect(() => {
    GetKnowledgeData();
    setstr('新建知识库');
  }, [page, pageSize, total]);

  const PagChange = (props: any) => {
    let { page, pageSize } = props;
    setPage(page);
    setPageSize(pageSize);
  };

  //模糊搜索
  const QueryFilterOnFinish = (values: any) => {
    settitle(values.creater);
    setstatus(values.sex);
    GetKnowledgeData();
  };
  // 设置一个状态管理上线/草稿切换
  const [Change, setChange] = useState(false);
  // 点击切换上线/草稿
  const change = () => {
    setChange(!Change);
  };
  return (
    <div
      style={{
        padding: 24,
      }}
    >
      {/* 上面的搜索选区 */}
      <QueryFilter<{
        name: string;
      }>
        //搜索案件的功能
        onFinish={async (values: any) => {
          await QueryFilterOnFinish(values);
        }}
      >
        <ProFormText name="creater" label="名称" />
        <ProFormSelect
          name="sex"
          label="状态"
          showSearch
          valueEnum={{
            published: '已发布',
            draft: '草稿',
          }}
        />
      </QueryFilter>
      {/* 下面的内容区域 */}
      <ProCard
        style={{
          height: '100vh',
          minHeight: 800,
        }}
      >
        <Button type="primary" className="button" onClick={showDrawer}>
          +新建
        </Button>
        <div className="knowledge-content">
          {/* 卡片 */}
          {data
            ? data.map((item: any, index: number) => {
                return (
                  <Card
                    key={item.id}
                    style={{ width: 340 }}
                    cover={
                      <img
                        alt="example"
                        src={item.cover}
                        style={{ width: 340, height: 130 }}
                      />
                    }
                    actions={[
                      //点击跳转页面
                      <EditOutlined
                        key="edit"
                        onClick={() =>
                          history.push(`/knowledgeBooklet/edit/${item.id}`)
                        }
                      />,
                      // <CloudUploadOutlined key="search" onClick={() => {}} />,
                      <div onClick={change}>
                        {Change ? (
                          <Tooltip title="发布线上">
                            <CloudDownloadOutlined key="cloudDown" />
                          </Tooltip>
                        ) : (
                          <Tooltip title="设为草稿">
                            <CloudUploadOutlined key="cloudDown" />
                          </Tooltip>
                        )}
                      </div>,
                      //点击设置
                      <SettingOutlined
                        key="setting"
                        onClick={() => {
                          setVal(item);
                          showDrawers();
                        }}
                      />,
                      //点击设置
                      <Popconfirm
                        title="确认删除？"
                        okText="确认"
                        cancelText="取消"
                        // 成功
                        onConfirm={() => {
                          message.success('Click on Yes');
                          delteKnow(item.id);
                          GetKnowledgeData();
                        }}
                      >
                        <DeleteOutlined key="delete" />,
                      </Popconfirm>,
                    ]}
                  >
                    <Meta title={item.title} description={item.summary} />
                  </Card>
                );
              })
            : '暂无数据'}
        </div>
      </ProCard>
      <UserPagination children={{ total, page, pageSize, PagChange }} />
      {/* 弹框组件 */}
      <Drawers opens={open} clone={onClose} titles={str} val={val} />
    </div>
  );
};
