import { FC } from 'react';
import style from './index.less';

// 首页
const Index: FC = () => {
  return <div className={style.IndexWrap}>Index</div>;
};

export default Index;
