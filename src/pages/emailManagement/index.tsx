import './index.less';
import { Table } from 'antd';
import { GetEmailData } from '@/api/emailApi';
import type { ColumnsType } from 'antd/es/table';
import React, { useState, useEffect } from 'react';
import UserPagination from '@/components/userPagination';
import { ProFormText, QueryFilter } from '@ant-design/pro-components';

interface DataType {
  key: React.Key;
  name: string;
  age: number;
  address: string;
}

const columns: ColumnsType<DataType> = [
  {
    title: '发件人',
    width: 300,
    dataIndex: 'name',
    key: 'name',
    // fixed: 'left',
  },
  {
    title: '收件人',
    width: 300,
    dataIndex: 'age',
    key: 'age',
    // fixed: 'left',
  },
  { title: '主题', width: 300, dataIndex: 'address', key: '1' },
  { title: '发送时间', width: 300, dataIndex: 'address', key: '2' },
  { title: '操作', width: 300, dataIndex: 'address', key: '3' },
];
const EmailPag = () => {
  const [data, setData] = useState([]);
  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  // page=1&pageSize=12&from=123123&to=qwe&subject=123
  const [from, setfrom] = useState(1);
  const [to, setto] = useState(1);
  const [subject, setsubject] = useState(1);
  const GetUserTableData = async () => {
    await GetEmailData({
      page,
      pageSize,
      from,
      to,
      subject,
    }).then((res) => {
      let arr = res.data[0];
      arr.forEach((item: any) => (item.key = item.id));
      setData(arr);
      setTotal(res.data[1]);
    });
  };

  useEffect(() => {
    GetUserTableData();
  }, [page, pageSize, total]);

  const PagChange = (props: any) => {
    let { page, pageSize } = props;
    setPage(page);
    setPageSize(pageSize);
  };

  return (
    <div
      style={{
        padding: 24,
      }}
    >
      {/* 上面的搜索选区 */}
      <QueryFilter<{
        name: string;
      }>
        //搜索案件的功能
        onFinish={async (values: any) => {
          console.log(values, '1111111');
          setfrom(values.from);
          setto(values.to);
          setsubject(values.subject);
          GetUserTableData();
          // page=1&pageSize=12&from=123123&to=qwe&subject=123
        }}
      >
        <ProFormText name="from" label="发件人" placeholder="请输入发件人" />
        <ProFormText name="to" label="收件人" placeholder="请输入收件人" />
        <ProFormText name="subject" label="主题" placeholder="请输入主题" />
      </QueryFilter>
      <Table columns={columns} dataSource={data} scroll={{ x: 1300 }} />
      <UserPagination children={{ total, page, pageSize, PagChange }} />
    </div>
  );
};

export default EmailPag;
