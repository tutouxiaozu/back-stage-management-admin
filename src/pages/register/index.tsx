import { FC } from 'react';
import style from './index.less';
import { Button, Form, Input, message } from 'antd';
import { _register } from '@/api/register/index';
import { Link, history } from 'umi';

interface RegisterData {
  name: string;
  password: string;
  confirm: string;
}

// 注册
const Register: FC = () => {
  const [form] = Form.useForm();
  const onFinish = async (values: RegisterData) => {
    const RegisterData = { name: values.name, password: values.password };
    // console.log(RegisterData)
    const data = await _register(RegisterData);
    console.log(data);
    if (data.statusCode == 201) {
      message.info('注册成功，快去登录吧');
      history.push('/login');
    }
  };
  return (
    <div className={style.Register_Wrap_Box}>
      <div className={style.Register_Wrap_Big_Box}>
        <div className={style.Register_Wrap_Left_Box}></div>
        <div className={style.Register_Wrap_Right_Box}>
          <div className={style.Register_Wrap_Title_Box}>
            <p>访客注册</p>
          </div>
          <div className={style.Register_Wrap_user_Box}>
            <Form
              labelCol={{ span: 2 }}
              wrapperCol={{ span: 20 }}
              initialValues={{ remember: true }}
              onFinish={onFinish}
              autoComplete="off"
            >
              <Form.Item
                name="name"
                label="账号"
                rules={[
                  {
                    required: true,
                    message: 'Please input your username!',
                  },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="password"
                label="密码"
                rules={[
                  {
                    required: true,
                    message: 'Please input your password!',
                  },
                ]}
                hasFeedback
              >
                <Input.Password />
              </Form.Item>

              <Form.Item
                name="confirm"
                label="确认"
                dependencies={['password']}
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: 'Please confirm your password!',
                  },
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (!value || getFieldValue('password') === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject(
                        new Error(
                          'The two passwords that you entered do not match!',
                        ),
                      );
                    },
                  }),
                ]}
              >
                <Input.Password />
              </Form.Item>

              <Form.Item labelCol={{ span: 15 }} wrapperCol={{ span: 22 }}>
                <Button type="primary" block htmlType="submit">
                  注册
                </Button>
              </Form.Item>
            </Form>
            <p>
              Or{' '}
              <span>
                <Link to="/login">去登录</Link>
              </span>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
