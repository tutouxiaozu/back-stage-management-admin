import { useEffect, useState } from 'react';
import { Popconfirm, Button, Input, Dropdown, Menu, message } from 'antd';
import { CloseOutlined, EllipsisOutlined } from '@ant-design/icons';
import { articleManager } from '../../api/Fa/index';
import style from './index.less';
import { useRequest, history } from 'umi';
const MdEditor = () => {
  const { run } = useRequest(articleManager, { manual: true });
  const [info, setInfo] = useState<any>({});
  const menu = (
    <Menu
      items={[
        {
          key: '1',
          label: <a href="https://www.antgroup.com">查看</a>,
          disabled: true,
        },
        {
          key: '2',
          label: <a>设置</a>,
          disabled: true,
        },
        {
          key: '3',
          label: (
            <a
              onClick={(e) => {
                e.preventDefault();
                if (!info.title) {
                  message.warn('请先设置标题');
                } else {
                  createOrudpArticle('draft');
                }
              }}
            >
              保存草稿
            </a>
          ),
        },
        {
          key: '4',
          label: <span>删除</span>,
          disabled: true,
        },
      ]}
    />
  );
  //标题表单框变化
  const hanleChange = (e: any) => {
    setInfo({
      ...info,
      title: e.target.value,
    });
  };
  //获取子iframe的值
  const getContent = ({ data }: any) => {
    setInfo({
      ...info,
      content: data,
      html: data,
    });
  };
  //发布或草稿文章
  const createOrudpArticle = async (status: string) => {
    await run({
      method: 'post',
      data: {
        ...info,
        status,
      },
    });
    message.success('发布文章成功');
    history.push('/');
  };
  const confirm = () => {
    history.push('/');
  };
  useEffect(() => {
    window.addEventListener('message', getContent, false);
    return () => {
      window.removeEventListener('message', getContent, false);
    };
  }, []);
  return (
    <div className={style.PageManagementWrap}>
      <div className={style.PageManagementHeader}>
        <div className={style.PageManagementTitle}>
          <Popconfirm
            title="确认关闭？如果有内容变更，请先保存。"
            onConfirm={confirm}
            okText="确认"
            cancelText="取消"
          >
            <Button className={style.Button} icon={<CloseOutlined />}></Button>
          </Popconfirm>
          <Input
            className={style.Input}
            placeholder="请输入文章标题"
            bordered={false}
            value={info.title}
            onChange={hanleChange}
          ></Input>
        </div>
        <div className={style.PageManagementRelease}>
          <Button
            className={style.Buttons}
            type="primary"
            onClick={() => {
              if (!info.title) {
                message.warn('请先设置标题');
              } else {
                createOrudpArticle('publish');
              }
            }}
          >
            发布
          </Button>
          <Dropdown className={style.Dian} overlay={menu}>
            <EllipsisOutlined />
          </Dropdown>
        </div>
      </div>
      <iframe
        width={1536}
        height={655}
        src="https://jasonandjay.com/editor/static/"
      />
    </div>
  );
};
export default MdEditor;
