import { FC } from 'react';
import { Button, Form, Input, message } from 'antd';
import style from './index.less';
import { _login } from '@/api/login';
import { useLocalStorageState } from 'ahooks';
import { useModel, history, Link } from 'umi';
const Login: FC = () => {
  const [, setUserInfo] = useLocalStorageState('user_Info');
  const { refresh } = useModel('@@initialState');
  const onFinish = async (values: any) => {
    // console.log('Success:', values);
    const { data } = await _login(values);
    setUserInfo({
      ...data,
    });
    // 让app下的getInitialState重新执行
    refresh();
    message.info('登陆成功');
    history.push('/workbench');
  };
  return (
    <div className={style.Login_Wrap_Box}>
      <div className={style.Login_Wrap_Big_Box}>
        <div className={style.Login_Wrap_Left_Box}></div>
        <div className={style.Login_Wrap_Right_Box}>
          <div className={style.Login_Wrap_Title_Box}>
            <p>系统登录</p>
          </div>
          <div className={style.login_Wrap_user_Box}>
            <Form
              name="登录系统"
              labelCol={{ span: 2 }}
              wrapperCol={{ span: 20 }}
              initialValues={{ remember: true }}
              onFinish={onFinish}
              autoComplete="off"
            >
              <Form.Item
                label="账号"
                name="name"
                rules={[
                  { required: true, message: 'Please input your username!' },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                label="密码"
                name="password"
                rules={[
                  { required: true, message: 'Please input your password!' },
                ]}
              >
                <Input.Password />
              </Form.Item>
              <Form.Item labelCol={{ span: 10 }} wrapperCol={{ span: 22 }}>
                <Button type="primary" block htmlType="submit">
                  登录
                </Button>
              </Form.Item>
            </Form>
            <p>
              Or{' '}
              <span>
                <Link to="/register">注册用户</Link>
              </span>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
