import { FC, useEffect } from 'react';
import style from './index.less';
import { Button } from 'antd';
import {
  Input,
  Select,
  message,
  Popconfirm,
  Modal,
  Table,
  Pagination,
  Form,
  Row,
  Col,
} from 'antd';
import type { ColumnsType } from 'antd/es/table';
import type { TableRowSelection } from 'antd/es/table/interface';
import React, { useState } from 'react';
import { _get_Page } from '@/api/articleManagerApi';
import { useRequest } from 'ahooks';
import { ReloadOutlined } from '@ant-design/icons';
import { request } from 'umi';
import { history } from 'umi';
import VisitCount from '@/components/visitCount/index';
interface DataType {
  key: React.Key;
  name: string;
  id: string;
  views: string;
}
// 页面管理页面
const PageManagement: FC = () => {
  const { data: pageData } = useRequest(_get_Page);
  const [form] = Form.useForm();
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(6);
  const [total, setTotal] = useState(1);
  const [arr, setArr] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);

  const changeItem = (record: any) => {
    request(`/api/api/page/${record.id}`, {
      method: 'PATCH',
      data: {
        status: record.status === 'publish' ? 'draft' : 'publish',
      },
    }).then((res) => {
      if (res.statusCode === 200) {
        _get_Page({ page, pageSize }).then((res) => {
          const arr = res.data[0];
          setArr(arr);
          setTotal(res.data[1]);
        });
      }
    });
  };
  //确定删除
  const confirm = (id: any) => {
    request(`/api/api/page/${id}`, {
      method: 'DELETE',
    }).then((res) => {
      if (res.statusCode === 200) {
        message.success('删除成功');
        _get_Page({ page, pageSize }).then((res) => {
          const arr = res.data[0];
          setArr(arr);
          setTotal(res.data[1]);
          setSelectedRowKeys([]);
        });
      }
    });
  };
  const cancel = () => {
    message.error('您已取消删除');
  };
  //批量删除
  const allDel = () => {
    selectedRowKeys.forEach((item: any) => {
      confirm(item);
    });
  };
  //批量更改状态
  const publish = () => {
    message.success('操作成功');
    selectedRowKeys.forEach((item: any) => {
      request(`/api/api/page/${item}`, {
        method: 'PATCH',
        data: {
          status: 'publish',
        },
      }).then((res) => {
        if (res.statusCode === 200) {
          _get_Page({ page, pageSize }).then((res) => {
            const arr = res.data[0];
            setArr(arr);
            setTotal(res.data[1]);
            setSelectedRowKeys([]);
          });
        }
      });
    });
  };
  const draft = () => {
    message.success('操作成功');
    selectedRowKeys.forEach((item: any) => {
      request(`/api/api/page/${item}`, {
        method: 'PATCH',
        data: {
          status: 'draft',
        },
      }).then((res) => {
        if (res.statusCode === 200) {
          _get_Page({ page, pageSize }).then((res) => {
            const arr = res.data[0];
            setArr(arr);
            setTotal(res.data[1]);
            setSelectedRowKeys([]);
          });
        }
      });
    });
  };
  const toEditor = (record: any) => {
    history.push({
      pathname: '/pageManagement/editor',
      query: {
        id: record.id,
      },
    });
  };
  //弹窗
  const showModal = (id: string) => {
    setIsModalOpen(true);
    const url = `www.com/article/${id}`;
    request(`/api/api/view/url/?url=${url}`, {
      method: 'GET',
    }).then((res) => {
      console.log(res);
    });
    // show_visit(url)
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const columns: ColumnsType<DataType> = [
    {
      title: '名称',
      dataIndex: 'name',
    },
    {
      title: '路径',
      dataIndex: 'path',
    },
    {
      title: '顺序',
      dataIndex: 'order',
    },
    {
      title: '阅读量',
      dataIndex: 'views',
      render: (views) => <span className={style.views}>{views}</span>,
    },
    {
      title: '状态',
      dataIndex: 'status',
      render: (status) => (
        <span>
          {status === 'publish' ? (
            <span>
              <b style={{ color: 'lightgreen' }}>●</b>已发布
            </span>
          ) : (
            <span>
              <b style={{ color: 'orange' }}>●</b>草稿
            </span>
          )}
        </span>
      ),
    },
    {
      title: '发布时间',
      dataIndex: 'publishAt',
    },
    {
      title: '操作',
      render: (record) => {
        return (
          <div className={style.action}>
            <span
              onClick={() => {
                toEditor(record);
              }}
            >
              编辑
            </span>
            <span onClick={() => changeItem(record)}>
              {record.status === 'publish' ? (
                <span>下线</span>
              ) : (
                <span>发布</span>
              )}
            </span>
            <span onClick={() => showModal(record.id)}>查看访问</span>
            <Popconfirm
              title="确认删除这个文章么?"
              onConfirm={() => confirm(record.id)}
              onCancel={cancel}
              okText="确定"
              cancelText="取消"
            >
              <span>删除</span>
            </Popconfirm>
          </div>
        );
      },
    },
  ];

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    // console.log('selectedRowKeys changed: ', selectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };
  // 当数据 没有选中的时候 就是禁用状态
  const hasSelected = selectedRowKeys.length > 0;
  const rowSelection: TableRowSelection<DataType> = {
    selectedRowKeys,
    onChange: onSelectChange,
    selections: [
      Table.SELECTION_ALL,
      Table.SELECTION_INVERT,
      Table.SELECTION_NONE,
      {
        key: 'odd',
        text: 'Select Odd Row',
        onSelect: (changableRowKeys) => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((_, index) => {
            if (index % 2 !== 0) {
              return false;
            }
            return true;
          });
          setSelectedRowKeys(newSelectedRowKeys);
        },
      },
      {
        key: 'even',
        text: 'Select Even Row',
        onSelect: (changableRowKeys) => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((_, index) => {
            if (index % 2 !== 0) {
              return true;
            }
            return false;
          });
          setSelectedRowKeys(newSelectedRowKeys);
        },
      },
    ],
  };
  //如果有值赋值给arr
  useEffect(() => {
    if (pageData) {
      setArr(pageData.data[0]);
    }
  }, [pageData]);

  useEffect(() => {
    _get_Page({ page, pageSize }).then((res) => {
      const arr = res.data[0];
      setArr(arr);
      setTotal(res.data[1]);
    });
  }, [page, pageSize]);

  const onFinish = (values: any) => {
    const brr = pageData.data[0].filter(
      (item: any) =>
        item.name.includes(values.name) ||
        item.path.includes(values.path) ||
        item.status.includes(values.status),
    );
    setArr(brr);
  };
  const reLoad = () => {
    location.reload();
  };

  return (
    <div className={style.PageManagementWrap}>
      <div
        className={style.search}
        style={{
          marginTop: '16px',
        }}
      >
        {/* 搜索 */}
        <Form form={form} onFinish={onFinish}>
          <Row>
            <Col span={8}>
              <Form.Item label={'标题'} name={'name'} className={style.form}>
                <Input placeholder="请输入标题" />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item label={'路径'} name={'path'} className={style.form}>
                <Input placeholder="请输入路径" />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item label={'状态'} name={'status'} className={style.form1}>
                <Select
                  options={[
                    {
                      value: 'publish',
                      label: '发布',
                    },
                    {
                      value: 'draft',
                      label: '草稿',
                    },
                  ]}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={2} offset={20}>
              <Form.Item className={style.button}>
                <Button type="primary" htmlType="submit">
                  搜索
                </Button>
                <Button
                  htmlType="button"
                  onClick={() => {
                    form.resetFields();
                  }}
                >
                  重置
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </div>
      <div className={style.table_list}>
        <li className={style.list_t}>
          <span className={hasSelected ? '' : style.top_button}>
            <Button onClick={() => publish()}>发布</Button>
            <Button onClick={() => draft()}>下线</Button>
            <Popconfirm
              title="确认删除这个文章么?"
              onConfirm={() => allDel()}
              onCancel={cancel}
              okText="确定"
              cancelText="取消"
            >
              <Button danger>删除</Button>
            </Popconfirm>
          </span>
          <span className={style.top_button2}>
            <Button
              type="primary"
              onClick={() => history.push('/pageManagement/editor')}
            >
              +新建
            </Button>
            <ReloadOutlined onClick={() => reLoad()} />
          </span>
        </li>
        {pageData && (
          <Table
            rowSelection={rowSelection}
            columns={columns}
            dataSource={arr}
            pagination={false}
            rowKey={(arr) => arr.id}
          />
        )}
        <div className={style.SearchRecordWrap_Pagination}>
          <Pagination
            total={total}
            showSizeChanger
            current={page}
            pageSize={pageSize}
            pageSizeOptions={[5, 10, 15, 20]}
            responsive={false}
            showTotal={(total) => `共 ${total} 条`}
            onChange={(page, pageSize) => {
              setPage(page);
              setPageSize(pageSize);
            }}
          />
        </div>
        <Modal
          title="访问统计"
          open={isModalOpen}
          onOk={handleOk}
          onCancel={handleCancel}
          footer={null}
        >
          <VisitCount></VisitCount>
        </Modal>
      </div>
    </div>
  );
};

export default PageManagement;
