import style from './index.less';
import { FC, useState, useEffect } from 'react';
import { SearchData, ColumnsTyle } from './index.d';
import UserPagination from '@/components/userPagination';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import SearchQueryFilter from '@/components/searchQueryFilter';
import { Button, Table, Space, message, Popconfirm } from 'antd';
import useBreadRoutes from '@/hooks/useBreadcrumb/useBreadcrumb';
import { GetSearchData, DeleteSearchItem } from '@/api/searchApi';

const breadcrumbRoutes = [
  { path: '/workbench', breadcrumbName: '工作台' },
  { path: '/searchRecord', breadcrumbName: '搜索记录' },
];

// 搜索记录页面
const SearchRecord: FC = () => {
  const [page, setPage] = useState(1);
  const [data, setData] = useState([]);
  const [type, setType] = useState('');
  const [total, setTotal] = useState(1);
  const [count, setCount] = useState('');
  const [keyword, setKeyword] = useState('');
  const [pageSize, setPageSize] = useState(10);
  const [loading, setLoading] = useState(false); // 点击 按钮模拟异步 loading 事件;
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);

  // 封装请求数据的方法
  const GetHttpsData = async () => {
    await GetSearchData({
      page,
      type,
      count,
      keyword,
      pageSize,
    }).then((res) => {
      let arr = res.data[0];
      arr.forEach((item: SearchData) => (item.key = item.id));
      setData(arr);
      setTotal(res.data[1]);
    });
  };

  // 进入页面请求数据
  useEffect(() => {
    GetHttpsData();
  }, [page, pageSize, total, keyword, count, type]);

  // 表格点击删除事件
  const start = () => {
    setLoading(true);
    AllDelete(selectedRowKeys);
    setTimeout(() => {
      setLoading(false);
      setSelectedRowKeys([]);
    }, 1000);
  };

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  // 当数据 没有选中的时候 就是禁用状态
  const hasSelected = selectedRowKeys.length > 0;

  // columns 是表格渲染用的所有的 类型
  const columns: ColumnsTyle[] = [
    {
      key: '1',
      title: '搜索词',
      dataIndex: 'keyword',
    },
    {
      key: '2',
      title: '搜索量',
      render: (record: SearchData) => (
        <span className={style.SearchRecordWrap_Count_Span}>
          {record.count}
        </span>
      ),
    },
    {
      key: '3',
      title: '搜索时间',
      render: (record: any) => <span>{record.createAt.toLocaleString()}</span>,
    },
    {
      key: '4',
      title: '操作',
      render: (record: SearchData) => (
        <Popconfirm
          title="确认删除这个搜索记录？"
          onConfirm={() => confirm(record)}
          onCancel={() => message.error('已经取消删除')}
          okText="Yes"
          cancelText="No"
        >
          <Space size="middle">
            <a className={style.SearchRecordWrap_a_style}>删除</a>
          </Space>
        </Popconfirm>
      ),
    },
  ];

  // 单项删除确认按钮
  const confirm = async (record: any) => {
    await DeleteSearchItem({
      id: record.id,
    });
    GetHttpsData();
    message.success('删除成功');
  };

  // 删除 选择的 选项
  const AllDelete = async (data: any) => {
    await data.forEach((item: any) => {
      setTimeout(() => {
        DeleteSearchItem({ id: item });
      }, 500);
    });
    setTimeout(() => {
      GetHttpsData();
    }, 1500);
  };

  const SearchOnFinish = (values: any) => {
    setKeyword(values.words);
    setCount(values.num);
    setType(values.type);
  };

  // 分页事件
  const PagChange = (props: any) => {
    let { page, pageSize } = props;
    setPage(page);
    setPageSize(pageSize);
  };

  return (
    <PageHeaderWrapper
      title={false}
      breadcrumb={useBreadRoutes(breadcrumbRoutes)}
    >
      <div className={style.SearchRecordWrap}>
        <div className={style.SearchRecordWrap_utils_header}>
          <SearchQueryFilter children={{ SearchOnFinish }} />
        </div>

        <div className={style.SearchRecordWrap_list_wrap}>
          {/* 数据展示表格 */}
          <div style={{ marginBottom: 16 }}>
            <Button
              danger
              type="primary"
              onClick={start}
              loading={loading}
              disabled={!hasSelected}
            >
              Delete
            </Button>
            <span style={{ marginLeft: 8 }}>
              {hasSelected ? `当前选中 ${selectedRowKeys.length} 记录` : ''}
            </span>
          </div>
          <Table
            rowSelection={rowSelection}
            columns={columns}
            dataSource={data}
            pagination={false}
          />
          <UserPagination children={{ total, page, pageSize, PagChange }} />
        </div>
      </div>
    </PageHeaderWrapper>
  );
};

export default SearchRecord;
