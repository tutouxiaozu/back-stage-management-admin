export interface SearchData {
  id: string;
  key?: string;
  count: number;
  keyword: string | undefined | null;
  createAt: string;
  type: string;
  updateAt: string;
}

export interface ColumnsTyle {
  key: string;
  title: string;
  dataIndex?: string;
  render?: any;
}
