/* 个人中心 */

import React, { FC } from 'react';
import { Row, Col, List } from 'antd';
import { useRequest } from 'ahooks';
import {
  getArticle,
  getCategory,
  getComment,
  getFile,
  getTags,
} from '@/api/myInfo/ownspach';
import MyInfo from '../../components/myInfo';
import style from './index.less';
const MyCenter: FC = () => {
  let { data: article } = useRequest(() =>
    getArticle({
      page: 1,
      pageSize: 10,
    }),
  );
  let { data: category } = useRequest(() => getCategory());
  let { data: tags } = useRequest(() => getTags());
  let { data: file } = useRequest(() =>
    getFile({
      page: 1,
      pageSize: 10,
    }),
  );
  let { data: comment } = useRequest(() =>
    getComment({
      page: 1,
      pageSize: 10,
    }),
  );
  const data = [
    `累计发表${article?.data[1]}文章`,
    `累计创建${category?.data.length}分类`,
    `累计创建${tags?.data.length}个标签`,
    `累计上传${file?.data[1]}文件`,
    `累计获得${comment?.data[1]}评论`,
  ];
  return (
    <div className={style.myCenter}>
      <main>
        <Row>
          <Col span={11} style={{ background: '#fff', height: '100%' }}>
            <List
              header={<div>系统概览</div>}
              bordered
              dataSource={data}
              renderItem={(item) => <List.Item>{item}</List.Item>}
            />
          </Col>
          <Col span={11} offset={1} style={{ background: '#fff' }}>
            <MyInfo />
          </Col>
        </Row>
      </main>
    </div>
  );
};
export default MyCenter;
