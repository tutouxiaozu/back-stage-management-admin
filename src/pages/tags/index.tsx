import { FC, useState } from 'react';
import style from './index.less';
import { useRequest } from 'ahooks';
import { _get_Tag } from '@/api/articleManagerApi';
import { add_Tag, del_Tag, edit_Tag } from '@/api/tagApi/index';
import { Button, message, Form, Input } from 'antd';
// 标签管理页面
const Label: FC = () => {
  const { data: tagData } = useRequest(_get_Tag);
  const [form] = Form.useForm();
  const [str, setStr] = useState('添加标签');
  const [title, setTitle] = useState('保存');
  const [obj, setObj] = useState([]);
  const [id, setId] = useState('');
  const [flag, setFlag] = useState(false);
  const onFinish = async (values: any) => {
    if (title === '保存') {
      const res = await add_Tag(values);
      if (res.statusCode === 201) {
        _get_Tag();
        form.setFieldsValue({
          label: '',
          value: '',
        });
        message.success('添加成功');
        location.reload();
      }
    } else {
      const res = await edit_Tag({
        ...obj,
        label: values.label,
        value: values.value,
      });
      if (res.statusCode === 200) {
        _get_Tag();
        form.setFieldsValue({
          label: '',
          value: '',
        });
        message.success('更新成功');
        location.reload();
      }
    }
  };
  const formItem = (item: any) => {
    form.resetFields();
    form.setFieldsValue(item);
    setStr('标签管理');
    setTitle('更新');
    setObj(item);
    setId(item.id);
    setFlag(true);
  };
  const delTag = async () => {
    const res = await del_Tag(id);
    if (res.statusCode === 200) {
      _get_Tag();
      form.setFieldsValue({
        label: '',
        value: '',
      });
      message.success('删除成功');
      location.reload();
    }
  };
  const fanhui = () => {
    form.resetFields();
    setFlag(false);
    setStr('添加标签');
    setTitle('保存');
  };
  return (
    <div className={style.labelManagementWrap}>
      <div className={style.addTag}>
        <h3>{str}</h3>
        <Form form={form} onFinish={onFinish} className={style.input}>
          <Form.Item label={''} name={'label'}>
            <Input placeholder="输入标签名称" />
          </Form.Item>

          <Form.Item label={''} name={'value'}>
            <Input placeholder="输入标签名(请输入英文,作为路由使用)" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              {title}
            </Button>
            <Button
              className={flag === false ? style.buttonNone : ''}
              onClick={() => fanhui()}
            >
              返回添加
            </Button>
            <Button
              onClick={() => delTag()}
              className={flag === false ? style.buttonNone : ''}
              danger
            >
              删除
            </Button>
          </Form.Item>
        </Form>
      </div>
      <div className={style.allTag}>
        <h3>所有分类</h3>
        <div className={style.list}>
          {tagData &&
            tagData.data.map((item: any, index: number) => {
              return (
                <span
                  key={index}
                  className={style.TagSpan}
                  onClick={() => formItem(item)}
                >
                  {item.label}
                </span>
              );
            })}
        </div>
      </div>
    </div>
  );
};

export default Label;
