import { FC } from 'react';
import './index.less';
import React, { useEffect, useState } from 'react';
import {
  SettingOutlined,
  DeleteOutlined,
  UnorderedListOutlined,
} from '@ant-design/icons';
import style from './index.less';
import { Button, Popover, Input, Popconfirm, message } from 'antd';
//引入数据
import { getKnow } from '@/api/knowledgeBooklet/index';
//富文本
// import ReactWEditor from 'wangeditor-for-react';
//引入抽离的弹框组件
import Drawers from '../../components/drawer/index';
import { useParams } from 'umi';
import { history } from 'umi';
//引入删除的接口
// import {deleteFile} from '@/api/file/index'

//点击卡片进入详情页面
const CardDitor: FC = (props: any) => {
  // const { opens, clone, titles, val } = props;
  const [str, setstr] = useState('新建知识库');
  const [open, setOpen] = useState(false);
  const [val, setVal] = useState('');
  const onClose = () => {
    setOpen(false);
  };
  const showDrawers = () => {
    // 点击设置
    setstr('更新知识库');
    setOpen(true);
  };
  //分页传的数据
  let [pagetion, setPagetion] = useState({
    page: 1,
    pageSize: 333,
  });
  // 接受id
  // 获取数据
  let { id } = useParams<{ id: string }>();
  let [reviewlist, setReviewlist] = useState([]);

  const get_list = async () => {
    let res = await getKnow({
      ...pagetion,
    });
    if (res.statusCode) {
      setReviewlist(res.data[0]);
    }
  };

  const [arrs, setArr] = useState<any>([]);
  //点击新添加的时候
  const [newval, setnewVal] = useState('');

  useEffect(() => {
    get_list();
  }, []);

  //回显数据
  const arr: any = reviewlist.filter((item: any) => item.id === id);

  const confirm = () => {
    message.success('Click on Yes');
    history.push('/knowledgebooklet');
    get_list();
  };

  //走接口的添加数据
  // const

  // 点击新建
  const newInfos = () => {
    let list = arrs;
    list.push(newval);
    setnewVal('');
    setArr(list);
  };

  // 新建卡片里面的内容
  const content = (
    <div className={style.text}>
      <span>
        <Input
          value={newval}
          onChange={(e) => {
            setnewVal(e.target.value);
          }}
        />
      </span>
      <span>
        <Button type="primary" onClick={() => newInfos()}>
          新建
        </Button>
      </span>
    </div>
  );

  return (
    <div className={style.cardditors}>
      <div className={style.knowdetail}>
        <div className={style.left}>
          <div className={style.top}>
            {/* 点击关闭 */}
            <Popconfirm
              title="确认关闭?如果有内容变更,请先保存。"
              onConfirm={() => confirm()}
              // onCancel={cancel}
              okText="确认"
              cancelText="取消"
            >
              <span>X</span>
            </Popconfirm>

            <div className={style.img}>
              <img src={arr[0] && arr[0].cover} alt="" />
              <span>{arr[0] && arr[0].title}</span>
            </div>
            {/* 设置 */}
            {/* <span
            onClick={() => showDrawers()}
            > */}
            <SettingOutlined
              onClick={() => {
                setVal(arr[0]);
                showDrawers();
              }}
            />
            {/* </span> */}
          </div>
          <div className={style.center}>
            <Button>保存</Button>
          </div>
          <div className={style.but}>
            <span>{arrs.length}篇文章</span>
            <Popover placement="right" content={content} trigger="click">
              <Button type="primary">新建</Button>
            </Popover>
          </div>

          <div className={style.list}>
            {arrs
              ? arrs.map((item: any, index: number) => {
                  return (
                    <div key={index} className={style.cover}>
                      <div className={style.div}>
                        <span>
                          <UnorderedListOutlined />
                          &emsp;{item}
                        </span>
                      </div>
                      {/* //删除 */}
                      <span>
                        <Popconfirm
                          title="确认删除？"
                          okText="确认"
                          cancelText="取消"
                          // 成功
                          onConfirm={() => {
                            message.success('Click on Yes');
                            // delteKnow(item.id);
                            // GetKnowledgeData();
                            arrs.splice(0, 1);
                            setArr(arrs);
                            console.log(arrs, '123');
                          }}
                        >
                          <DeleteOutlined />
                        </Popconfirm>
                      </span>
                    </div>
                  );
                })
              : ''}
          </div>
        </div>
        <div className={style.right}>请新建章节 (或者选择章节进行编辑)</div>
        {/* 弹框组件 */}
        <Drawers opens={open} clone={onClose} titles={str} val={val} />
      </div>
    </div>
  );
};

export default CardDitor;
