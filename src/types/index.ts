// 文件
export * from './modules/file';
// 邮件
export * from './modules/mail';

export interface userData {
  id: string;
  avatar: string;
  email: string;
  role: string;
  status: boolean;
  token: string;
  name: string;
  updateAt: string;
}
export interface updatePswData extends userData {
  oldPassword: string;
  newPassword: string;
}
//评论
export interface commentData {
  content: string;
  createAt: string;
  email: string | null;
  hostId: string;
  html: string;
  id: string;
  name: string;
  parentCommentId: string | null;
  pass: boolean;
  replyUserEmail: string | null;
  replyUserName: string | null;
  updateAt: string;
  url: string;
  userAgent: string;
  key?: string;
}
export interface tableParams {
  page: number;
  pageSize: number;
}
//回复
export interface replyItemData {
  content: string;
  createAt: string;
  email: string;
  hostId: string;
  html: string;
  id: string;
  name: string;
  parentCommentId: string | null;
  pass: boolean;
  replyUserEmail: string | null;
  replyUserName: string | null;
  updateAt: string;
  url: string;
  userAgent: string;
}
export interface replyData {
  content: string;
  createByAdmin: boolean;
  email: string;
  hostId: string;
  name: string;
  parentCommentId: string;
  replyUserEmail: string;
  replyUserName: string;
  url: string;
}
//搜索
export interface searchData {
  name?: string;
  email?: string;
  pass?: number;
  page: number;
  pageSize?: number;
}
