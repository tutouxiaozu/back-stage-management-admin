// request 进行 二次 封装;
/*
    1. request 统一配置,处理请求的公共参数,
    1.1 比如所 携带token(身份标识), csrf-token
    2. 对错误的统一处理
*/
// Re questConfig 请求配置
// import { response } from 'express';
import style from './css/app.less';
import { RequestConfig, history } from 'umi';
import { Button, Dropdown, Menu, message } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { ProBreadcrumb } from '@ant-design/pro-layout';
import UserInformation from '@/components/userInformation';
import FooterLayout from './components/footerLayout/FooterLayout';

interface Headers {
  authorization?: string | undefined | null;
  [propName: string]: any;
}

// 启用 initial-state 插件
export const getInitialState = () => {
  // 该函数 在页面初始化的时候 执行一次,并且 他只会执行一次;
  // 需要获取初始化的状态,
  // 容错 处理
  try {
    const userInfo = JSON.parse(localStorage.getItem('user_Info') as string); // 获取本地存储中的用户信息;
    if (!userInfo) {
      // 如果用户不存在 那么就让他登录
      history.replace('/login');
    }
    return userInfo;
  } catch (error) {
    console.log(error, 'error');
  }
};

// layout 封装;
export const layout = () => {
  const menu = (
    <Menu
      onClick={({ key, domEvent }) => {
        // domEvent 获取原生事件
        domEvent.stopPropagation(); // 阻止事件的冒泡行为；
        history.push(key);
      }}
      items={[
        {
          key: '/article/amEditor',
          label: '新建文章-协同编辑器',
        },
        {
          key: '/article/editor',
          label: '新建文章',
        },
        {
          key: '/pageManagement/editor',
          label: '新建页面',
        },
      ]}
    ></Menu>
  );

  return {
    menuHeaderRender: (logo: string, title: string) => {
      return (
        <div>
          {logo}
          {title}
          <Dropdown arrow overlay={menu} placement="bottom">
            <Button
              type="primary"
              className={style.layoutButton}
              icon={<PlusOutlined />}
              onClick={(e: any) => e.stopPropagation()}
            >
              新建
            </Button>
          </Dropdown>
        </div>
      );
    },
    footerRender: () => {
      return <FooterLayout />;
    },
    headerContentRender: () => <ProBreadcrumb />,
    rightContentRender: () => <UserInformation />,
  };
};

const TokenBearer = 'Bearer';
export const request: RequestConfig = {
  timeout: 20000,
  errorConfig: {
    // 为错误信息,配置一些自定义提示...
    adaptor: (resData: any) => {
      console.log('修改他的相应结果,使用 adaptor', resData);
      return {
        ...resData,
        errorMessage: resData.msg, // sussess 为false的时候,使用弹框提示errormsg信息
      };
    },
  },

  // 请求拦截器
  requestInterceptors: [
    // url:请求的路径; options:请求的参数;
    (url: any, options: any) => {
      console.log(
        '接口请求前执行,处理请求接口的时候携带的公共参数(options)',
        options,
      );
      const headers: Headers = {
        ...options.headers,
      };
      // 然后判断 权限有没有(就是有没有token值);
      if (!options.isAuthorization) {
        // 判断是否有权限
        // 如果没有权限的话,获取本地的token值
        headers['authorization'] = `${TokenBearer} ${
          localStorage.getItem('user_Info') &&
          JSON.parse(localStorage.getItem('user_Info') as string).token
        }`;
      }
      return {
        url: `${url}`, // 请求地址
        options: {
          ...options,
          interceptors: true,
          headers,
        },
      };
    },
  ],

  // 相应拦截
  responseInterceptors: [
    async (response: any) => {
      const res = await response.json(); // 将相应信息转化成json格式
      switch (res.statusCode) {
        case 403:
          res.msg = `您暂时没有该接口权限`;
          break;
        case 401:
          history.replace('/login');
          break;
        case 200:
          break;
      }
      return res;
    },
  ],
};
