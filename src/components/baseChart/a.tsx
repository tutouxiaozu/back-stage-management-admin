import { FC } from 'react';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { LoginFormPage, ProFormText } from '@ant-design/pro-components';
import { _login } from '@/api/login';
import type { CSSProperties } from 'react';
import { useLocalStorageState } from 'ahooks';

import { useDispatch } from 'dva';
import { useModel, history } from 'umi';
import style from './css/index.less';
const iconStyles: CSSProperties = {
  color: 'rgba(0, 0, 0, 0.2)',
  fontSize: '18px',
  verticalAlign: 'middle',
  cursor: 'pointer',
};

interface LoginData {
  name: string;
  password: string;
}

// 登录页面
const Login: FC = () => {
  const dispatch = useDispatch();
  const [, setUserInfo] = useLocalStorageState('userInfo');
  const { refresh } = useModel('@@initialState');
  const handClickLogin = async (value: LoginData) => {
    const { data } = await _login(value);
    // console.log(data);
    setUserInfo({
      ...data,
    });
    // 让app下的getInitialState重新执行
    refresh();
    history.push('/workbench');
  };
  return (
    <div className={style.LoginWrap}>
      <div className={style.LoginWrap_box}>
        <div
          style={{
            backgroundColor: 'white',
            height: 'calc(100vh - 48px)',
            margin: -24,
          }}
        >
          <LoginFormPage
            backgroundImageUrl="https://gw.alipayobjects.com/zos/rmsportal/FfdJeJRQWjEeGTpqgBKj.png"
            subTitle="系统登录"
            onFinish={handClickLogin}
            style={style.nameInput}
          >
            <>
              <ProFormText
                name="name"
                labelAlign="left"
                label="账号"
                fieldProps={{
                  size: 'large',
                  prefix: <UserOutlined className={'prefixIcon'} />,
                }}
                placeholder={'用户名: admin or user'}
                rules={[
                  {
                    required: true,
                    message: '请输入用户名!',
                  },
                ]}
              />
              <ProFormText.Password
                name="password"
                labelAlign="left"
                label="密码"
                fieldProps={{
                  size: 'large',
                  prefix: <LockOutlined className={'prefixIcon'} />,
                }}
                placeholder={'密码: ant.design'}
                rules={[
                  {
                    required: true,
                    message: '请输入密码！',
                  },
                ]}
              />
            </>
          </LoginFormPage>
        </div>
      </div>
    </div>
  );
};
export default Login;
