import { useState } from 'react';
import { Avatar, Input, Button } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { updateUser } from '@/api/myInfo/ownspach';
import '../../pages/personalPage/index.less';
function Setting() {
  const user = JSON.parse(localStorage.getItem('user_Info') as string);
  const [name, setName] = useState(user.name);
  const [email, setEmail] = useState(
    user.email ? user.email : '3160721510@qq.com',
  );
  console.log(user);

  return (
    <div>
      <div style={{ width: '100%' }} className="imgbox">
        {user && user.avatar ? (
          <div className="logoimgs" style={{ width: '65px', height: '65px' }}>
            <img
              src={user.avatar}
              alt=""
              style={{ width: '100%', height: '100%', borderRadius: '50%' }}
            />
          </div>
        ) : (
          <Avatar size="large" icon={<UserOutlined />} />
        )}
      </div>
      <div>
        <p style={{ display: 'flex' }} className="poste">
          <span>{'用户名'}:</span>
          <Input value={name} onChange={(e) => setName(e.target.value)} />
        </p>
        <p style={{ display: 'flex' }} className="poste">
          <span>{'邮箱'}:</span>
          <Input value={email} onChange={(e) => setEmail(e.target.value)} />
        </p>
      </div>
      <Button
        onClick={() => {
          user.email = email;
          user.name = name;
          updateUser({ ...user }).then((res) => {
            localStorage.setItem('user', JSON.stringify(res.data));
          });
        }}
      >
        保存
      </Button>
    </div>
  );
}

export default Setting;
