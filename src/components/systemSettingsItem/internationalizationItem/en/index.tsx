import MonacoEditor from '@monaco-editor/react';
import { useEffect, useState, Fragment } from 'react';
import { GetSystemSettingsData } from '@/api/systemSettingsApi';

const EnReactUeditor = (props: any) => {
  const [code, setCode] = useState<any>();

  useEffect(() => {
    GetSystemSettingsData().then((res) => {
      let value =
        res.data.i18n && JSON.parse(res.data.i18n)['中文']
          ? JSON.parse(res.data.i18n)['中文']
          : JSON.parse(res.data.i18n)['英文'];
      let arr = value && JSON.stringify(value).split(',');
      let data = arr
        .map((item: any) => {
          return item + ',\n';
        })
        .join('');
      setCode(data);
    });
  }, []);

  const options = {
    // readOnly: true, //是否只读
    automaticLayout: true, //自动布局
    wordWrap: true, //折行展示
    // colors: {
    // 'editor.background': '#fff', //编辑器背景色
    // 'editor.lineHighlightBorder': '#E7F2FD', // 编辑行边框色
    // },
    side: 'right',
    // selectOnLineNumbers: true,
    // renderSideBySide: false,
    // scrollBeyondLastLine: false,
    // formatOnPaste: true,
    // contextmenu: false, // 禁止右键
    // fixedOverflowWidgets: true, // 超出编辑器大小的使用fixed属性显示
    // quickSuggestions: false, // 默认的提示关掉
    // minimap: {
    //     // 缩略图
    //     enabled: false,
    // },
    // scrollbar: {
    //     // 滚动条
    //     horizontalScrollbarSize: 6,
    //     verticalScrollbarSize: 6,
    // },
    // lineNumbersMinChars: 3, // 最少显示3位长的行号
    // lineNumbers: 'on', // 是否显示行号
  };

  function onChangeHandle(value: any) {
    setCode(value);
  }

  function handleEditorWillMount(monaco: any) {
    // monaco 挂载后的钩子 参数为 monaco实例 , 在这里可以做一些定制化的操作
  }

  function handleEditorDidMount() {
    // 卸载后 这个钩子里可以做销毁monaco...操作
  }

  return (
    <Fragment>
      <div
        className="editor-container"
        style={{ height: '400px', width: '100%' }}
      >
        <MonacoEditor
          loading={<div>loading...</div>}
          theme="dark" // 主题
          // width="400"
          // height="600"
          language="json" // 编辑器支持的语言格式
          value={code} // 编辑器中要展示的代码
          options={options}
          onChange={onChangeHandle}
          beforeMount={handleEditorWillMount}
          onMount={handleEditorDidMount}
        ></MonacoEditor>
      </div>
    </Fragment>
  );
};

export default EnReactUeditor;
