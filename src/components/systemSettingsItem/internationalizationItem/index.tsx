import { FC, useEffect } from 'react';
import { Tabs, Button } from 'antd';
import EnReactUeditor from './en/index';

// 国际化设置
const InternationalizationItem: FC = ({ children: { childrenData } }: any) => {
  const initialItems = [
    { label: 'en', children: <EnReactUeditor />, key: '1' },
  ];

  return (
    <div style={{ padding: '20px' }}>
      <Tabs type="editable-card" items={initialItems} />
      <Button type="primary" style={{ margin: '10px' }}>
        保存
      </Button>
    </div>
  );
};

export default InternationalizationItem;
