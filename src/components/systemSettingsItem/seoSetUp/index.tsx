import { FC } from 'react';
import style from './index.less';
import { Button, Form, Input } from 'antd';
import { SystemSettingsSava } from '@/api/systemSettingsApi';

const { TextArea } = Input;

// SEO设置
const SetSetup: FC = ({ children }: any) => {
  const [form] = Form.useForm();
  form.setFieldsValue({
    seoKeyword: children.seoKeyword,
    seoDesc: children.seoDesc,
  });

  const onFinish = async (values: any) => {
    await SystemSettingsSava(values);
  };

  return (
    <div className={style.SeoSetUp_Warp}>
      <Form layout={'vertical'} onFinish={onFinish} form={form}>
        <Form.Item label="关键词" name="seoKeyword">
          <Input placeholder="input placeholder" />
        </Form.Item>
        <Form.Item label="描述信息" name="seoDesc">
          <TextArea rows={4} />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            保存
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default SetSetup;
