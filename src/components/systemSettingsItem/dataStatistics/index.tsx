import { FC } from 'react';
import style from './index.less';
import { Button, Form, Input } from 'antd';
import { SystemSettingsSava } from '@/api/systemSettingsApi';

// 数据统计
const DataStatistics: FC = ({ children }: any) => {
  const [form] = Form.useForm();
  form.setFieldsValue({
    baiduAnalyticsId: children.baiduAnalyticsId,
    googleAnalyticsId: children.googleAnalyticsId,
  });

  const onFinish = async (values: any) => {
    await SystemSettingsSava(values);
  };

  return (
    <div className={style.DataSetUp_Wrap}>
      <Form layout={'vertical'} onFinish={onFinish} form={form}>
        <Form.Item label="百度统计" name="baiduAnalyticsId">
          <Input placeholder="input placeholder" />
        </Form.Item>
        <Form.Item label="谷歌统计" name="googleAnalyticsId">
          <Input placeholder="input placeholder" />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            保存
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default DataStatistics;
