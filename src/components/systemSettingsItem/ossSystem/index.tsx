import style from './index.less';
import { Alert, Button, message } from 'antd';
import { systemOSS } from '@/api/ossSystemApi';
import MonacoEditor from '@monaco-editor/react';
import { FC, useEffect, useState, Fragment } from 'react';

const OssSystem: FC = (props: any) => {
  let {
    children: { childrenData },
  } = props;
  const [code, setCode] = useState<any>();

  useEffect(() => {
    setCode(childrenData.oss);
  }, [childrenData]);

  function onChangeHandle(value: any) {
    setCode(value);
  }

  function handleEditorWillMount(monaco: any) {
    // monaco 挂载后的钩子 参数为 monaco实例 , 在这里可以做一些定制化的操作
  }

  function handleEditorDidMount() {
    // 卸载后 这个钩子里可以做销毁monaco...操作
  }

  return (
    <div className={style.OssSystem_Wrap}>
      <Alert
        message="说明"
        description='请在编辑器中输入您的 oss 配置，并添加 type 字段区分 {"type":"aliyun","accessKeyId":"","accessKeySecret":"","bucket":"","https":true,"region":""}'
        type="info"
        showIcon
      />
      {code && (
        <Fragment>
          <div
            className="editor-container"
            style={{ height: '400px', width: '100%' }}
          >
            <MonacoEditor
              loading={<div>loading...</div>}
              theme="dark" // 主题
              // width="400"
              // height="600"
              language="json" // 编辑器支持的语言格式
              value={code} // 编辑器中要展示的代码
              options={{ selectOnlineNumbers: true }}
              onChange={onChangeHandle}
              beforeMount={handleEditorWillMount}
              onMount={handleEditorDidMount}
            ></MonacoEditor>
          </div>
        </Fragment>
      )}
      <Button
        type="primary"
        onClick={async () => {
          let params = (childrenData.oss = code);
          await systemOSS(params);
          message.success('保存成功');
        }}
      >
        保存
      </Button>
    </div>
  );
};

export default OssSystem;
