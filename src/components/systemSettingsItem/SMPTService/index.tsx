// SMPTService
import { FC, useEffect } from 'react';
import style from './index.less';
import { Button, Form, Input } from 'antd';
import { SystemSettingsSava } from '@/api/systemSettingsApi';

const SMPTService: FC = ({ children }: any) => {
  console.log(
    children,
    '--------------------------------------------------------',
  );

  const [form] = Form.useForm();
  useEffect(() => {
    form.setFieldsValue({
      smtpHost: children.smtpHost,
      smtpPort: children.smtpPort,
      smtpUser: children.smtpUser,
      smtpPass: children.smtpPass,
      smtpFromUser: children.smtpFromUser,
    });
  }, [children, form]);

  const onFinish = async (values: any) => {
    await SystemSettingsSava(values);
  };

  return (
    <div className={style.SMPTService_Wrap}>
      <Form layout={'vertical'} onFinish={onFinish} form={form}>
        <Form.Item label="SMTP 地址" name="smtpHost">
          <Input placeholder="请输入SMTP地址 " />
        </Form.Item>
        <Form.Item label="SMPT 端口（强制使用SLL连接）" name="smtpPort">
          <Input placeholder="请输入SMTP端口号" />
        </Form.Item>
        <Form.Item label="SMTP 用户" name="smtpUser">
          <Input placeholder="请输入SMTP用户名" />
        </Form.Item>
        <Form.Item label="SMPT 密码" name="smtpPass">
          <Input placeholder="请输入SMTP密码" />
        </Form.Item>
        <Form.Item label="发件人" name="smtpFromUser">
          <Input placeholder="请输入邮箱" />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            保存
          </Button>
          &emsp;
          <Button type="dashed" htmlType="submit">
            测试
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default SMPTService;
