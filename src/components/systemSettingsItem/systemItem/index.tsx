import { FC, useEffect } from 'react';
import style from './index.less';
import { Button, Form, Input } from 'antd';
import { SystemSettingsSava } from '@/api/systemSettingsApi';

const { TextArea } = Input;

// Tab系统设置
const SystemItem: FC = ({ children }: any) => {
  const [form] = Form.useForm();

  useEffect(() => {
    form.setFieldsValue({
      systemUrl: children.systemUrl,
      adminSystemUrl: children.adminSystemUrl,
      systemTitle: children.systemTitle,
      systemLogo: children.systemLogo,
      systemFavicon: children.systemFavicon,
      systemFooterInfo: children.systemFooterInfo,
    });
  }, [form, children]);

  const onFinish = async (values: any) => {
    await SystemSettingsSava(values);
  };

  return (
    <div className={style.SystemItem_Wrap}>
      <Form layout={'vertical'} onFinish={onFinish} form={form}>
        <Form.Item label="系统地址" name="systemUrl">
          <Input placeholder="input placeholder" />
        </Form.Item>
        <Form.Item label="后台地址" name="adminSystemUrl">
          <Input placeholder="input placeholder" />
        </Form.Item>
        <Form.Item label="系统标题" name="systemTitle">
          <Input placeholder="input placeholder" />
        </Form.Item>
        <Form.Item label="Logo" name="systemLogo">
          <Input placeholder="input placeholder" />
        </Form.Item>
        <Form.Item label="Favicon" name="systemFavicon">
          <Input placeholder="input placeholder" />
        </Form.Item>
        <Form.Item label="页脚信息" name="systemFooterInfo">
          <TextArea rows={4} />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            保存
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default SystemItem;
