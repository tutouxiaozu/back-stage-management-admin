import { FC } from 'react';
import style from './index.less';
import { GithubOutlined, CopyrightOutlined } from '@ant-design/icons';

const FooterLayout: FC = () => {
  return (
    <div className={style.footerGithubWrap}>
      <div>
        <GithubOutlined
          className="footer-github"
          onClick={() => (window.location.href = 'https://github.com/')}
        />
      </div>
      <div>
        <span>
          Copyright
          <CopyrightOutlined className="footer-CopyrightOutlined" />
          2022 Designed by Fantasticit.
        </span>
      </div>
    </div>
  );
};

export default FooterLayout;
