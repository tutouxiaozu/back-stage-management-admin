import { FC } from 'react';
import { ProFormText, QueryFilter } from '@ant-design/pro-components';

const SearchQueryFilter: FC = (props: any) => {
  const {
    children: { SearchOnFinish },
  } = props;

  return (
    <QueryFilter
      defaultCollapsed
      split
      onFinish={async (values) => {
        console.log(1111);
        if (values) await SearchOnFinish(values);
      }}
    >
      <ProFormText name="type" label="类型" placeholder={'请输入搜索类型'} />
      <ProFormText name="words" label="搜索词" placeholder={'请输入搜索词'} />
      <ProFormText name="num" label="搜索量" placeholder={'请输入搜索量'} />
    </QueryFilter>
  );
};

export default SearchQueryFilter;
