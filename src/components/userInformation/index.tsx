import { FC } from 'react';
import style from './index.less';
import { GithubOutlined, UserOutlined } from '@ant-design/icons';
import { Avatar, Image, Dropdown, Menu, Space } from 'antd';
import { useModel, Link } from 'umi';

const menu: any = (
  <Menu
    items={[
      {
        key: '1',
        label: <Link to="/personalCenter">个人中心</Link>,
      },
      {
        key: '2',
        label: <Link to="/">用户管理</Link>,
      },
      {
        key: '3',
        label: <Link to="/">系统设置</Link>,
      },
      {
        key: '4',
        label: <Link to="/login">退出登录</Link>,
      },
    ]}
  />
);

const UserInformation: FC = () => {
  const { initialState } = useModel('@@initialState');

  return (
    <div className={style.UserInformation_wrap}>
      <span>
        <GithubOutlined className={style.UserInformation_wrap_github_icon} />
      </span>
      <Space direction="vertical">
        <Space wrap>
          <Dropdown overlay={menu} placement="bottom">
            <span>
              {initialState &&
              initialState.avatar !== null &&
              initialState.avatar ? (
                <Avatar
                  src={
                    <Image src={initialState.avatar} style={{ width: 32 }} />
                  }
                />
              ) : (
                <Avatar
                  style={{ backgroundColor: '#87d068' }}
                  icon={<UserOutlined />}
                />
              )}
            </span>
          </Dropdown>
        </Space>
      </Space>
      <span className={style.UserInformation_wrap_user_information}>
        {initialState && initialState.name ? `Hi，${initialState.name}` : ''}
      </span>
    </div>
  );
};

export default UserInformation;
