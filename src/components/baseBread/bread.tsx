// @ts-nocheck
import { Link, useLocation } from 'react-router-dom';
import { Breadcrumb } from 'antd';

const breadcrumbNameMap = {
  '/': '工作台',
  '/alticle': '文章管理',
  '/alticle/allArticle': '所有文章',
  '/alticle/classify': '分类管理',
  '/alticle/label': '标签管理',
  '/pageManager': '页面管理',
  '/comment': '评论管理',
  '/knowledge': '知识小册',
  '/email': '邮件管理',
  '/file': '文件管理',
  '/search': '搜索记录',
  '/access': '访问统计',
  '/user': '用户管理',
  '/poster': '海报管理',
  '/system': '系统设置',
};

const Bread = () => {
  const location = useLocation();
  const pathSnippets = location.pathname.split('/').filter((i) => i);
  const extraBreadcrumbItems = pathSnippets.map((_, index) => {
    const url = `/${pathSnippets.slice(0, index + 1).join('/')}`;
    return (
      <Breadcrumb.Item key={url}>
        <Link to={url}>{breadcrumbNameMap[url]}</Link>
      </Breadcrumb.Item>
    );
  });
  const breadcrumbItems = [
    <Breadcrumb.Item key="home">
      <Link to="/">工作台</Link>
    </Breadcrumb.Item>,
  ].concat(extraBreadcrumbItems);

  return (
    <div className="demo">
      <Breadcrumb style={{ padding: '14px 0', background: '#fff' }}>
        {breadcrumbItems}
      </Breadcrumb>
    </div>
  );
};

export default Bread;
