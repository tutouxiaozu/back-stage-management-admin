import { Modal, Input, Form, Button } from 'antd';
import style from './index.less';

const { TextArea } = Input;

interface Props {
  setIsModalOpen: boolean;
  isShowModal: boolean;
  clickCancel(): void;
  clickReply(value: any): void;
}

const BaseModal = (props: Props) => {
  let { isShowModal, clickCancel, clickReply } = props;
  // console.log(props);

  const sureReply = (value: any) => {
    clickReply(value);
  };
  return (
    <>
      <Modal
        title="回复评论"
        visible={isShowModal}
        footer={null}
        onCancel={clickCancel}
        // onOk={handleOk}
      >
        <Form onFinish={sureReply}>
          <Form.Item name={'text'}>
            <TextArea rows={6} placeholder="支持Markdown" />
          </Form.Item>
          <Form.Item className={style.btns}>
            <Button onClick={clickCancel}>取消</Button>
            <Button type="primary" htmlType="submit">
              回复
            </Button>
          </Form.Item>
        </Form>
        {/* <TextArea rows={6} placeholder="支持Markdown" /> */}
      </Modal>
    </>
  );
};

export default BaseModal;
