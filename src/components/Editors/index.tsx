import style from './index.less';
import { Card } from 'antd';
import { Image } from 'antd';
const gridStyle: React.CSSProperties = {
  width: '25%',
  textAlign: 'center',
};
function Editors(props: any) {
  const { file } = props;
  return (
    <Card title="Card Title">
      {file &&
        file.map((item: any) => {
          return (
            <Card.Grid className={style.grid} style={gridStyle} key={item.id}>
              <p>
                <Image
                  className={style.imgs}
                  width={100}
                  height={100}
                  src={item.url}
                />
              </p>
              <p className={style.title}>{item.originalname}</p>
            </Card.Grid>
          );
        })}
    </Card>
  );
}

export default Editors;
