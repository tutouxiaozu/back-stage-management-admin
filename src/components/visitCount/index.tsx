import { FC, useEffect, useState } from 'react';
import style from './index.less';
import * as echarts from 'echarts';
import { _get_Article } from '@/api/articleManagerApi/index';
import { useRequest } from 'ahooks';
const VisitCount: FC = function (props: any) {
  const { data: articleData } = useRequest(_get_Article);
  const [arr, setArr] = useState([]);
  useEffect(() => {
    if (articleData) {
      setArr(articleData.data[0]);
    }
  }, [articleData]);
  console.log(arr, '111111');

  arr.forEach((item: any) => {
    let str = [{}];
    str.push(item.views);
    // console.log(str,"999999999999999999");
  });

  const echart = () => {
    const chartDom = document.getElementById('main');
    const myChart = echarts.init(chartDom as any);
    const symbolSize = 20;
    const data = [
      [0, 100],
      [60, 200],
      [80, 150],
      [100, 30],
      [150, 201],
    ];
    const option = {
      title: {
        text: '访问统计',
        left: 'center',
      },
      tooltip: {
        triggerOn: 'none',
        formatter: function (params: any) {
          return (
            'X: ' +
            params.data[0].toFixed(2) +
            '<br>Y: ' +
            params.data[1].toFixed(2)
          );
        },
      },
      grid: {
        top: '8%',
        bottom: '12%',
      },
      xAxis: {
        min: 0,
        max: 300,
        type: 'value',
        axisLine: { onZero: false },
      },
      yAxis: {
        min: 0,
        max: 300,
        type: 'value',
        axisLine: { onZero: false },
      },
      dataZoom: [
        {
          type: 'slider',
          xAxisIndex: 0,
          filterMode: 'none',
        },
        // {
        //     type: 'slider',
        //     yAxisIndex: 0,
        //     filterMode: 'none'
        // },
        {
          type: 'inside',
          xAxisIndex: 0,
          filterMode: 'none',
        },
        // {
        //     type: 'inside',
        //     yAxisIndex: 0,
        //     filterMode: 'none'
        // }
      ],
      series: [
        {
          id: 'a',
          type: 'line',
          smooth: true,
          symbolSize: symbolSize,
          data: data,
        },
      ],
    };
    option && myChart.setOption(option);
  };
  useEffect(() => {
    echart();
  }, []);
  return <div id="main" className={style.visit}></div>;
};

export default VisitCount;
