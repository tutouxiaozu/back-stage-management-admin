import { FC } from 'react';
import {
  ProFormText,
  QueryFilter,
  ProFormSelect,
} from '@ant-design/pro-components';

const UserQueryFilter = (props: any) => {
  const { QueryFilterOnFinish } = props;

  return (
    <QueryFilter
      defaultCollapsed
      split
      span={4}
      onFinish={async (values) => await QueryFilterOnFinish(values)}
    >
      <ProFormText name="name" label="账户" placeholder={'请输入账户'} />
      <ProFormText name="email" label="邮箱" placeholder={'请输入邮箱'} />
      <ProFormSelect
        name="role"
        label="角色"
        showSearch
        valueEnum={{
          admin: '管理员',
          locked: '访客',
        }}
      />
      <ProFormSelect
        name="status"
        label="角色"
        showSearch
        valueEnum={{
          visitor: '锁定',
          active: '可用',
        }}
      />
    </QueryFilter>
  );
};

export default UserQueryFilter;
