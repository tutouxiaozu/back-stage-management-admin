import { FC } from 'react';
import style from './index.less';
import { Pagination } from 'antd';

const UserPagination: FC = function (props: any) {
  const {
    children: { total, page, pageSize, PagChange },
  } = props;

  return (
    <div className={style.UserManagementWrap_Table_Pagination}>
      <Pagination
        total={total}
        showSizeChanger
        current={page}
        pageSize={pageSize}
        pageSizeOptions={[5, 10, 15, 20]}
        responsive={false}
        showTotal={(total: any) => `共 ${total} 条`}
        onChange={(page, pageSize) => PagChange({ page, pageSize })}
      />
    </div>
  );
};

export default UserPagination;
