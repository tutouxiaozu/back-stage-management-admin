import { useRef } from 'react';
import { Form, Input, Button, Row, Col } from 'antd';
import style from './editors.less';
import './index.css';
type Props = {
  sendSearchInfo: (obj: object) => void;
};

const EmainForm = (props: Props) => {
  let setName = useRef(null) as any;
  let setType = useRef(null) as any;
  // 搜索
  let onFinish = () => {
    // 获取搜索框内容
    let originalname = setName.current.input.value;
    let type = setType.current.input.value;
    // 子向父传一个事件
    props.sendSearchInfo({ name: originalname, types: type });
    console.log(props.sendSearchInfo);
  };

  // 重置
  let resetList = () => {
    props.sendSearchInfo({ name: '', types: '' });
  };
  return (
    <Form
      name="basic"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={{ remember: true }}
      onFinish={onFinish}
      onReset={resetList}
      autoComplete="off"
      layout="inline"
      className={style.publicForm}
    >
      <Row style={{ width: '100%', alignContent: 'center' }}>
        <Col span={7}>
          <Form.Item
            label="文件名称"
            name="publisher"
            style={{ width: '100%' }}
          >
            <Input placeholder="请输入文件名称" id="nameId" ref={setName} />
          </Form.Item>
        </Col>
        <Col span={7}>
          <Form.Item label="文件类型" name="getter" style={{ width: '100%' }}>
            <Input placeholder="请输入文件类型" id="typeId" ref={setType} />
          </Form.Item>
        </Col>
      </Row>
      <Row style={{ width: '100%', justifyContent: 'flex-end' }}>
        <Form.Item wrapperCol={{ offset: 11, span: 16 }}>
          <Button type="primary" htmlType="submit">
            搜索
          </Button>
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button htmlType="button" onClick={() => resetList()}>
            重置
          </Button>
        </Form.Item>
      </Row>
    </Form>
  );
};

export default EmainForm;
