import { FC } from 'react';
import React, { useState, useEffect } from 'react';
import {
  ProFormSelect,
  ProFormText,
  QueryFilter,
} from '@ant-design/pro-components';
import {
  EditOutlined,
  SettingOutlined,
  CloudUploadOutlined,
  DeleteOutlined,
  InboxOutlined,
} from '@ant-design/icons';
//引入样式
import './index.less';
import { Card, Button, Drawer, Input, Switch, Upload, Form, Space } from 'antd';
//引入卡片的数据
import { _file_ } from '@/api/file/index';
//分页的数据
import UserPagination from '@/components/userPagination';
//跳转页面
import { history } from 'umi';
//添加数据的接口
import { addKnow, getKnow, deitKnow } from '@/api/knowledgeBooklet/index';

import { useRequest } from 'ahooks';

const { TextArea } = Input;
const { Dragger } = Upload;

const Drawers = (props: any) => {
  const { opens, clone, titles, val } = props;
  const [open, setOpens] = useState(false);
  const [childrenDrawer, setChildrenDrawer] = useState(false);

  //请求数据
  //
  const [page, setPage] = useState(1); //分页
  const [pageSize, setPageSize] = useState(12); ///分页
  const [total, setTotal] = useState(1); //分页
  const [MoarkImg, setMoarkImg] = useState([]); //分页数据
  const [originalname, setoriginalname] = useState<any>('');
  const [type, settype] = useState<any>('');
  //设置第一个窗口回显的数据
  const [urls, setutls] = useState('');
  const [data, setData] = useState<any>([]);

  useEffect(() => {
    setOpens(opens);
    GetUserTableData();
  }, [page]);

  //弹框的初始化
  // 经 Form.useForm() 创建的 form 控制实例，不提供时会自动创建
  const [form] = Form.useForm();

  useEffect(() => {
    if (titles === '新建知识库') {
      form.setFieldsValue({
        title: '',
        summary: '',
        isCommentable: '',
        cover: '',
      });
    } else {
      form.setFieldsValue({
        title: val.title,
        summary: val.summary,
        isCommentable: val.isCommentable,
        cover: val.cover,
      });
    }
  }, [titles]);

  //获取第二个弹框里面的卡片数据
  const GetUserTableData = async () => {
    await _file_({
      page,
      pageSize,
      originalname,
      type,
    }).then((res) => {
      let arr = res.data[0];
      setMoarkImg(arr);
      setTotal(res.data[1]);
    });
  };

  //往数据里面添加数据
  //获取数据
  // 变为手动模式
  const { runAsync } = useRequest(getKnow, {
    manual: true,
  });

  // 请求数据的方法
  const GetKnowledgeData = () => {
    // ()()自己调用自己
    (async () => {
      let arr = await runAsync({
        page,
        pageSize,
      });
      setData(arr.data[0]);
      setTotal(arr.data[1]);
    })();
  };

  // 点击关闭
  const clones = () => {
    clone(false);
  };
  useEffect(() => {
    setOpens(opens);
  });
  //开关
  const onChange = (checked: boolean) => {
    console.log(`switch to ${checked}`);
  };
  const showChildrenDrawer = () => {
    setChildrenDrawer(true);
  };
  const onChildrenDrawerClose = () => {
    setChildrenDrawer(false);
  };

  //卡片图片
  const gridStyle: React.CSSProperties = {
    textAlign: 'center',
  };

  const PagChange = (props: any) => {
    let { page, pageSize } = props;
    setPage(page);
    setPageSize(pageSize);
  };

  //模糊搜索
  const QueryFilterOnFinish = (values: any) => {
    setoriginalname(values.creater);
    settype(values.sex);
    GetUserTableData();
  };

  const [getValue, setgetValue] = useState({
    title: '',
    summary: '',
    cover: urls,
  });
  // console.log(getValue, 'getValuegetValuegetValue');

  const onFinishs = async (values: any) => {
    if (titles === '新建知识库') {
      await addKnow(values);
      GetKnowledgeData();
      clones();
    } else if (titles === '更新知识库') {
      await deitKnow({
        ...values,
        id: val.id,
      });
      // GetKnowledgeData();
      clones();
    }
  };

  const onReset = () => {
    clones();
  };

  //卡片图片
  // const gridStyle: React.CSSProperties = {
  //   width: '300px',
  //   height: '160px',
  //   textAlign: 'center',
  // };
  return (
    <div>
      {/* 抽屉弹框 */}
      <Drawer
        title={titles}
        width={620}
        placement="right"
        onClose={clones}
        open={opens}
      >
        <Form form={form} onFinish={onFinishs}>
          <Form.Item label="名称" name="title">
            <Input size="middle" className="inputname" />
          </Form.Item>
          <Form.Item label="描述" name="summary">
            <TextArea rows={4} className="inputname" />
          </Form.Item>

          <Form.Item label="评论">
            <Switch defaultChecked onChange={onChange} />
          </Form.Item>

          <Form.Item label="封面">
            <Dragger className="dragBox" {...props}>
              {titles === '新建知识库' ? (
                urls ? (
                  <img
                    src={urls}
                    alt="avatar"
                    style={{ width: '470px', height: '200px' }}
                  />
                ) : (
                  <div>
                    <p className="ant-upload-drag-icon">
                      <InboxOutlined />
                    </p>
                    <p className="ant-upload-text">
                      点击选择文件或将文件拖拽到此处
                    </p>
                    <p className="ant-upload-hint">
                      文件将上传到OSS，如未配置请先配置
                    </p>
                  </div>
                )
              ) : (
                <img
                  src={val.cover}
                  alt="avatar"
                  style={{ width: '470px', height: '200px' }}
                />
              )}
            </Dragger>
          </Form.Item>

          <Form.Item name="cover">
            <Input size="middle" className="inputname2" />
          </Form.Item>

          <Form.Item>
            <Button className="buttons" onClick={showChildrenDrawer}>
              选择文件
            </Button>
          </Form.Item>
          <br />
          <hr />
          <Form.Item className="items">
            <Button htmlType="button" onClick={onReset}>
              取消
            </Button>
            {titles === '新建知识库' ? (
              <Button type="primary" htmlType="submit">
                保存
              </Button>
            ) : (
              <Button type="primary" htmlType="submit">
                更新
              </Button>
            )}
          </Form.Item>
        </Form>

        <Drawer
          title="文件选择"
          width={820}
          onClose={onChildrenDrawerClose}
          open={childrenDrawer}
        >
          {/* 上面的搜索选区 */}
          <QueryFilter<{
            name: string;
          }>
            //搜索案件的功能
            onFinish={async (values: any) => {
              await QueryFilterOnFinish(values);
            }}
          >
            <ProFormText name="creater" label="名称" />
            <ProFormSelect
              name="sex"
              label="状态"
              showSearch
              valueEnum={{
                published: '已发布',
                draft: '草稿',
              }}
            />
          </QueryFilter>
          {/* 上传的按钮 */}
          <Upload {...props}>
            <Button style={{ float: 'right' }}> 上传文件 </Button>
          </Upload>
          {/* 卡片里面的图片 */}
          <div className="xiapictue">
            <Card>
              {MoarkImg &&
                MoarkImg.map((item: any, index: number) => {
                  return (
                    <Card.Grid
                      style={gridStyle}
                      key={index}
                      onClick={() => {
                        form.setFieldsValue({
                          cover: item.url,
                        });
                        setutls(item.url);
                        //关闭弹框
                        onChildrenDrawerClose();
                      }}
                    >
                      <p className="img">
                        <img src={item.url} alt="" className="url" />
                      </p>
                      <p className="text">{item.originalname}</p>
                    </Card.Grid>
                  );
                })}
            </Card>
            {/* 分页组件的拆分 */}
            <UserPagination children={{ total, page, pageSize, PagChange }} />
          </div>
        </Drawer>
      </Drawer>
    </div>
  );
};

export default Drawers;
