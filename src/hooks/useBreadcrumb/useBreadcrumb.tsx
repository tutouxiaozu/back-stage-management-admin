import { Link } from 'umi';

const useBreadRoutes = (breadcrumbRoutes: any) => {
  const breadRoutes: any = {
    routes: breadcrumbRoutes,
    itemRender: (route: any, params: any, routes: any, paths: any) => {
      const secondRoute = routes.indexOf(route) === 0;
      return secondRoute ? (
        <Link to={route.path} style={{ color: 'rgba(0,0,0,0.65)' }}>
          {route.breadcrumbName}
        </Link>
      ) : (
        <span>{route.breadcrumbName}</span>
      );
    },
  };

  return breadRoutes;
};

export default useBreadRoutes;
