import { useCallback, useEffect, useState } from 'react';

const useWinSize = () => {
  // 初始化高度宽度
  const [size, setSize] = useState({
    width: document.documentElement.clientWidth,
    height: document.documentElement.clientHeight,
  });
  const get_size = useCallback(() => {
    setSize({
      width: document.documentElement.clientWidth,
      height: document.documentElement.clientHeight,
    });
  }, []);
  useEffect(() => {
    window.addEventListener('resize', get_size);
    return () => {
      window.removeEventListener('resize', get_size);
    };
  }, []);
  return size;
};

export default useWinSize;
