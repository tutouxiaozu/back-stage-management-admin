const routes: any = [
  {
    path: '/',
    redirect: '/workbench',
  },
  {
    path: '/home',
    component: '@/pages/index/index',
    menuRender: false,
    headerRender: false,
    title: '首页',
  },
  {
    path: '/login',
    component: '@/pages/login/index',
    menuRender: false,
    headerRender: false,
    footerRender: false,
    title: '登录',
    //跳路由的时候打开新的页面
    target: '_blank',
  },
  {
    path: '/register',
    component: '@/pages/register/index',
    menuRender: false,
    headerRender: false,
    footerRender: false,
    title: '注册',
    target: '_blank',
  },
  {
    path: '/workbench',
    component: '@/pages/workbench/index',
    name: '工作台',
    icon: 'DashboardOutlined',
    // hideInMenu: false,
  },
  {
    path: '/article',
    name: '文章管理',
    icon: 'FormOutlined',
    routes: [
      {
        path: '/article/allArticle',
        component: '@/pages/workbench/allarticle/index',
        name: '所有文章',
        icon: 'FormOutlined',
        // Routes: ['./router.ts'],
      },
      {
        path: '/article/classification',
        component: '@/pages/classification/index',
        name: '分类管理',
        icon: 'CopyOutlined',
      },
      {
        path: '/article/label',
        component: '@/pages/tags/index',
        name: '标签管理',
        icon: 'TagOutlined',
      },
      {
        path: '/article/amEditor',
        component: '@/pages/amEditor/index',
        headerRender: false,
        // 不展示页脚
        footerRender: false,
        // 不展示菜单
        menuRender: false,
        // 不展示菜单顶栏
      },
      {
        path: '/article/editor',
        component: '@/pages/editor/index',
        headerRender: false,
        footerRender: false,
        menuRender: false,
      },
    ],
  },
  {
    path: '/pageManagement',
    component: '@/pages/pageManagement/index',
    name: '页面管理',
    icon: 'CopyOutlined',
  },
  {
    path: '/pageManagement/editor',
    component: '@/pages/pageEditor/index',
    headerRender: false,
    footerRender: false,
    menuRender: false,
  },
  {
    path: '/knowledgeBooklet',
    component: '@/pages/knowledgeBooklet/index',
    name: '知识小册',
    icon: 'ContainerOutlined',
  },
  //知识小册的点击打开页面
  {
    path: '/knowledgeBooklet/edit/:id',
    component: '@/pages/knowledgeBooklet-detail/index',
    title: '知识小册',
    icon: 'ContainerOutlined',
    footerRender: false,
    headerRender: false,
    menuRender: false,
    target: '_blank',
  },
  {
    path: '/poster',
    component: '@/pages/poster/index',
    name: '海报管理',
    icon: 'CustomerServiceOutlined',
  },
  {
    path: '/comment',
    component: '@/pages/comment/index',
    name: '评论管理',
    icon: 'MessageOutlined',
  },
  {
    path: '/emailManagement',
    component: '@/pages/emailManagement/index',
    name: '邮件管理',
    icon: 'MailOutlined',
  },
  {
    path: '/fileManagement',
    component: '@/pages/fileManagement/index',
    name: '文件管理',
    icon: 'FolderOpenOutlined',
    footerRender: false,
  },
  {
    path: '/searchRecord',
    component: '@/pages/searchRecord/index',
    name: '搜索记录',
    icon: 'SearchOutlined',
  },
  {
    path: '/accessStatistics',
    component: '@/pages/accessStatistics/index',
    name: '访问统计',
    icon: 'ProjectOutlined',
  },
  {
    path: '/userManagement',
    component: '@/pages/userManagement/index',
    name: '用户管理',
    icon: 'UserOutlined',
  },
  {
    path: '/systemSettings',
    component: '@/pages/systemSettings/index',
    name: '系统管理',
    icon: 'SettingOutlined',
  },
  {
    path: '/personalCenter',
    component: '@/pages/personalPage/index',
    name: '个人中心',
    icon: 'SettingOutlined',
    hideInMenu: true,
  },
  {
    // path: '/user',
    // component: '@/pages/userManagement/index',
    // name: '用户管理',
    // access: 'isAdmin',
    // 在渲染组建之前判断 access.isAdmin 是否为true
    // 为 true 的时候渲染页面,如果为 false 的时候 返回 403
  },
];

export default routes;
