import { defineConfig } from 'umi';
import routes from './router';
import RootStyle from '../src/css/rootStyle';

export default defineConfig({
  title: '秃头五人组的后台管理',
  routes,
  layout: {
    name: '后台管理系统',
    logo: 'https://bwcreation.oss-cn-beijing.aliyuncs.com/2022-09-05/2.gif',
    siderWidth: 200,
    disableMobile: true,
  },
  proxy: {
    '/api': {
      target: 'https://creationadmin.shbwyz.com/',
      changeOrigin: true,
      pathRewrite: { '^/api': '' },
    },
  },
  styles: RootStyle,
  metas: [
    {
      name: 'keywords',
      content: 'umi, umijs',
    },
    {
      name: 'description',
      content: '🍙 插件化的企业级前端应用框架。',
    },
    {
      bar: 'foo',
    },
  ],
});
